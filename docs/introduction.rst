============
Introduction
============
**Version du Jbrowse 2** : 1.6.4

**Génomes disponibles** : Riesling pseudomolécules et haplotigs, et Gewurztraminer pseudomolécules et haplotigs.

**Jbrowse 2 production** : `http://147.100.144.109:5000/ <http://147.100.144.109:5000/>`_

**Jbrowse 2 développement** : `http://147.100.144.109:3000/ <http://147.100.144.109:3000/>`_

**Documentation Jbrowse 2 GMOD** : `https://jbrowse.org/jb2/docs/ <https://jbrowse.org/jb2/docs/>`_

.. _installation:

Installation
------------
Le projet **jbrowse-components**, disponible sur `GitHub <https://github.com/GMOD/jbrowse-components>`_, contient le code source du Jbrowse 2 et fournit de nombreux outils permettant de l'installer et de le maintenir, par exemple. La première étape est donc de cloner le dépôt sur le serveur et de lancer un **yarn** pour installer toutes les dépendances du projet :

.. code-block:: bash

	git clone https://github.com/GMOD/jbrowse-components.git
	cd jbrowse-components
	yarn

.. _Jbrowse CLI:

Jbrowse CLI (Command Line Interface)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
**Dossier** : ``/data2/malahaye/jbrowse-components/products/jbrowse-cli/``

Le CLI est un outil en ligne de commande, pouvant être utilisé pour installer un nouveau Jbrowse 2, le mettre à jour, ou encore le configurer :

.. code-block:: bash

	npm install -g @jbrowse/cli

La version la plus récente sera installée. L'installation peut être testée avec la commande suivante, qui renvoie le numéro de version (dans notre cas *@jbrowse/cli/1.4.4 linux-x64 node-v14.18.0*) :

.. code-block:: bash

	jbrowse --version

.. _Jbrowse 2:

Jbrowse 2
~~~~~~~~~

.. _Développement:

Développement
*************
**Dossier** : ``/data2/malahaye/jbrowse-components/products/jbrowse-web/``

Ensuite, le Jbrowse peut être installé en version développeur sur le port 3000. Les données qui seront visualisées, ainsi que le fichier de configuration *config.json* doivent être placés dans le dossier ``jbrowse-web/public/`` :

.. code-block:: bash

	npm start

La version de développement est maintenant accessible à l'adresse : `http://147.100.144.109:3000/ <http://147.100.144.109:3000/>`_

.. _Production:

Production
**********
**Dossier** : ``/data2/malahaye/jbrowse-components/products/jbrowse-web/``

La version de production peut ensuite être construite à partir de la version la plus récente du code source. Une sorte d'image est créée dans le dossier ``jbrowse-web/build/``, cela permettra de faire des modifications dans le code source qui ne seront visualisables que sur la version de développement, ce qui n'impactera pas la version de production. Avant de construire la version de production, il est important de déplacer le dossier ``public/data/`` contenant toutes les données, car sinon elles seront copiées dans le dossier ``build/`` :

.. code-block:: bash

	npm run build

**Dossier** : ``/data2/malahaye/jbrowse-components/products/jbrowse-web/build/``

Enfin, cette version peut être déployée sur le port 5000, en se plaçant dans le dossier contenant la version de production créée à l'étape précédente :

.. code-block:: bash

	npx serve -p 5000 . &

La version de production est maintenant accessible à l'adresse : `http://147.100.144.109:5000/ <http://147.100.144.109:5000/>`_

*Note* : Penser à ouvrir le port qui sera utilisé par le Jbrowse 2 : 3000 pour la version développeur, et 5000 pour la version production.

.. _Mise a jour:

Mise à jour
-----------
**Dossier** : ``/data2/malahaye/jbrowse-components/``

Pour mettre à jour le projet **jbrowse-components** avec la nouvelles *release* ou récupérer au fur et à mesure les corrections qui sont apportées dans le code, il faut simplement faire un **pull** du dépôt git. Puis, on peut se remettre sur le commit correspondant à la dernière *release* (optionnel). Il est important ensuite de lancer un **yarn** pour installer toutes les potentielles nouvelles dépendances, ou versions de dépendances utilisées dans ce projet :

.. code-block:: bash

	git pull
	git reset --hard <commit-id-release>
	yarn

**Dossier** : ``data2/malahaye/jbrowse-components/products/jbrowse-web/``

Seule la version de développement va être impactée, si l'on souhaite que la version de production soit mise à jour, il faut construire une nouvelle version (en pensant à d'abord déplacer le dossier ``data/`` autre part le temps le temps que la version de production soit créée) :

.. code-block:: bash

	npm run build

*Note* : Il n'est pas nécessaire de déployer à nouveau la version de production.

.. _GitLab:

GitLab
------
Certains fichiers sont suivis sur le dépôt à l'adresse suivante : `https://gitlab.com/MarieLahaye/jbrowse2 <https://gitlab.com/MarieLahaye/jbrowse2>`_, le dossier suivi localement est localisé ici : ``/data2/malahaye/git_Jbrowse2/``. Quelques fichiers, tels que du code source ou les fichiers de configuration, sont des liens physiques (git ne peut pas suivre correctement des liens symboliques) créés à partir des fichiers sources, car ils sont indispensables au bon fonctionnement du jbrowse. Il est donc nécessaire (lorsque ces fichiers sont créés à nouveau, par exemple) de recréer ces liens physiques, pour que les modifications faites dans le fichier d'origine soient visibles sur le fichier suivi par git.
