=========================
Organisation du Jbrowse 2
=========================
**Dossier projet jbrowse-components** : ``/data2/malahaye/jbrowse-components/``

Ce dossier est associé au dépôt git et va permettre de faire des pull pour récupérer les futures mises à jour des différents packages, permettant notamment de mettre en place un Jbrowse 2.

**Dossier suivi sur GitLab** : ``/data2/malahaye/git_jbrowse2/``

Ce répertoire est associé à un dépôt git hébergé sur GitLab : `https://gitlab.com/MarieLahaye/jbrowse2 <https://gitlab.com/MarieLahaye/jbrowse2>`_.

.. _Dossier jbrowse_components:

Dossier ``jbrowse-components/``
-------------------------------
**Dossier** : ``/data2/malahaye/jbrowse-components/``

Il contient différents *products*, qui sont des application telles que *jbrowse-web*, *jbrowse-desktop* ou le *jbrowse-cli*. 

**Dossier** : ``/data2/malahaye/jbrowse-components/products/jbrowse-web/``

On retrouve dans ce dossier uniquement ce qui est associé à l'interface web du Jbrowse 2. Dedans, se trouvent les dossiers ``public\`` et ``build\``, qui correspondent aux version de développement et de production du Jbrowse 2, respectivement. 

**Version de développement** : accessible via le port 3000, l'interface se recharge dès qu'une modification est faite (cf. :ref:`mettre en place version développement <Développement>`).

**Version de production** : accessible via le port 5000, cette version crée une sorte d'image de la version de l'interface à l'instant où a été construite cette version. Donc toutes modifications du code source ne sera pas prise en compte (cf. :ref:`mettre en place version production <Production>`).

.. _Fichier de configuration:

Fichiers et dossier importants pour le Jbrowse 2
------------------------------------------------
**Dossier** : ``/data2/malahaye/jbrowse-components/products/jbrowse-web/build/`` et ``/data2/malahaye/jbrowse-components/products/jbrowse-web/public/``

Fichier de configuration ``config.json``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Ce fichier de configuration au format JSON permet de gérer les tracks et les différens assemblages de tout le Jbrowse 2, et pas seulement d'un organisme, comme c'est le cas pour l'ancienne version du Jbrowse. Dans ce fichier de configuration vont être gérées les différentes options de visualisation des tracks. Contrairement aux données qui sont exactement les mêmes pour la version de développement et la version de production (le dossier ``data/`` est donc un lien symbolique pour ne pas dupliquer les données), le fichier de configuration des deux version n'est pas relié par des liens symboliques ou physiques, afin que l'on puisse faire des modifications dans la configuration des tracks sur la version de développement sans impacter la version de production.

Le *jbrowse-cli* permet de gérer les tracks, mais le script :ref:`initialization_tracks.py <initialization_tracks>` a été développé pour créer des modèles pour chaque tracks qui ont été ajoutées au Jbrowse. Cela permet de cadrer l'organisation des dossiers des données et les schémas des tracks.

.. _Organisation du dossier data:

Dossier ``data/``
~~~~~~~~~~~~~~~~~
**Dossier** ``/data2/malahaye/jbrowse-components/products/jbrowse-web/public/data/``

Ce dossier regroupe toutes les données nécessaires au Jbrowse 2. A noter, que dans la version de production, il s'agit d'un lien symbolique vers le dossier localisé dans la version de développement. Les données sont séparées par cépages (Riesling ou Gewurztraminer), puis séparées en fonction des données associées aux pseudomolécules et celles associées aux haplotigs. Les données sont donc organisées en fonction des Jbrowse auxquels elles sont associées (par exemple *Riesling pseudomolecules* ou *Gewurztraminer haplotigs*). Ensuite, les données sont organisées dans des dossiers correspondant aux catégories dans lesquelles sont regroupées les tracks sur le Jbrowse.

Arborescence du dossier :

.. code-block:: bash 

	data
	├── Gewurztraminer
	│	├── haplotigs
	│	└── pseudomolecules
	├── Riesling
	│	├── haplotigs
	│	│	├── 0._Reference_Sequence
	│	│	│	├── Riesling_haplotigs.fa
	│	│	│	├── Riesling_haplotigs.fa.fai
	│	│	│	└── Riesling_haplotigs.fa.mmi
	│	│	├── 1._Annotations
	│	│	│	├── 1.1_Riesling_annotations
	│	│	│	└── 1.2_PN12Xv2_annotations
	│	│	├── 2._RNA-Seq
	│	│	│	├── 2.1_Alignments
	│	│	│	├── 2.2_BigWig_XY
	│	│	│	├── 2.3_Sashimi_plot
	│	│	│	└── merged_RNA-Seq
	│	│	├── 3._WGS
	│	│	│	├── resequencing
	│	│	│	├── Riesling_resequencing_data.haplotigs.sorted.filtered.bam
	│	│	│	└── Riesling_resequencing_data.haplotigs.sorted.filtered.bam.bai
	│	│	└── RI.haplotigs.sizes
	│	└── pseudomolecules
	│	    ├── 0._Reference_Sequence
	│	    │	├── Riesling_pseudomolecules.ctgP.fa
	│	    │	├── Riesling_pseudomolecules.ctgP.fa.fai
	│	    │	└── Riesling_pseudomolecules.ctgP.fa.mmi
	│	    ├── 1._Annotations
	│	    │	├── 1.1_Riesling_annotations
	│	    │	└── 1.2_PN12Xv2_annotations
	│	    ├── 2._RNA-Seq
	│	    │	├── 2.1_Alignments
	│	    │	├── 2.2_BigWig_XY
	│	    │	├── 2.3_Sashimi_plot
	│	    │	├── merged_RNAseq
	│	    ├── 3._WGS
	│	    │	├── resequencing
	│	    │	├── Riesling_resequencing_data.pseudomolecules.sorted.filtered.bam
	│	    │	└── Riesling_resequencing_data.pseudomolecules.sorted.filtered.bam.bai
	│	    ├── 4._Haplotigs
	│	    │	├── alignments_cds
	│	    │	├── alignments_haplotigs
	│	    │	├── Alleles.sorted.bed.gz
	│	    │	├── Alleles.sorted.bed.gz.tbi
	│	    │	├── Haplotigs.sorted.bed.gz
	│	    │	└── Haplotigs.sorted.bed.gz.tbi
	│	    ├── 5._Others
	│	    │	├── contigs
	│	    │	├── Contigs.sorted.bb
	│	    │	├── N_gaps
	│	    │	└── N_gaps.sorted.bb
	│	    ├── RI.chrom.sizes
	│	    └── RI_metadata.json
	└── scripts -> /data2/malahaye/git_jbrowse2/public/data/scripts/

*Note* : Pour un soucis de lisibilité, l'arborescence s'arrête au niveau 4 de profondeur du répertoire ciblé. L'arborescence du dossier contenant les données de Gewurztraminer n'est pas détaillée, mais elle est identique à celle de Riesling.

.. _Nommage des dossiers et fichiers:

Nommage des dossier et fichiers finaux
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
La localisation et la manière dont sont nommés les fichiers finaux sur lesquels seront basées les tracks, sont très importantes. Le script :ref:`initialization_tracks.py <initialization_tracks>` a été développé pour s'assurer que l'organisation des tracks reste stable et éviter les erreurs. Ce script doit être modifié à chaque fois que l'on souhaite créer de nouvelles catégories ou sous-catégories de tracks, ce qui le rend peu permissif, mais permet de donner un cadrage sur la manière dont doivent être organisées les données.

Pour le moment, seules 5 catégories sont possibles : 
	* *0. Reference Sequence*
	* *1. Annotations*
	* *2. RNA-Seq*
	* *3. WGS*
	* *4. Haplotigs*
	* *5. Others*

Le script va regarder l'extension du fichier, ainsi que le nom du dossier dans lequel est localisé le fichier pour déterminer quelle track créer et quelle catégorie indiquer. Le nom du fichier correspondra au nom de la track, et le nom du répertoire le nom de la catégorie ou de la sous catégorie.

**Exemple 1** : Le fichier *Riesling_resequencing_data.haplotigs.sorted.filtered.bam* localisé dans le dossier ``3._WGS`` (comme on peut le voir dans l'arborescence ci-dessus). Le nom de la track sera *Riesling_resequencing_data*, et elle appartiendra à la catégorie *3. WGS*.

**Exemple 2** : Il est aussi possible d'avoir des tracks dans des sous-catégories. Le fichier ``RI_S1_571.sorted.bam`` est localisé dans les dossiers : ``2._RNA-Seq/2.1_Alignments/``. Le nom de la track sera *RI_S1_571*, et cette fois-ci, elle appartiendra à la catégorie *2. RNA-Seq* et à la sous-catégorie *2.1 Alignments*.

Comme vous l'aurez remarqué, le nom de la track prendra tout ce qui est situé avant le premier point dans le nom du fichier. Donc en général, on séparera par des points les différentes analyses qui ont été réalisées sur les données, cela permet de garder une trace, sans impacter le nom de la track ensuite.

.. _Dossier git_jbrowse2:

Dossier ``git_jbrowse2``
------------------------
Associé à un autre dépôt git que celui du **jbrowse-components**, il permet de suivre les fichiers associés à la documentation technique, les fichiers contenant du code source qui a été modifié, les fichiers de configuration et les scripts développés pour réaliser le pré-traitement des données.

Arborescence du dossier :

.. code-block:: bash

	data
	├── build
	│	└── config.json
	├── docs
	│	├── index.rst
	│	├── integration_donnees.rst
	│	├── introduction.rst
	│	├── organisation_jbrowse2.rst
	│	└── scripts.rst
	├── public
	│	├── config.json
	│	└── data
	│	    └── scripts
	└── src_to_follow
	    ├── formatting_gff3_and_generating_index.sh
	    ├── packages-core-util-jexl.ts
	    ├── Snakefile_GREAT_haplotigs
	    └── Snakefile_GREAT_pseudomolecules


Dans les dossiers ``build/`` et ``public/``, on varetrouver les fichiers de configuration des versions de production et de développement du Jbrowse 2. Dans le dossier ``public/scripts/`` on retrouve aussi tous les :ref:`scripts <Scripts>` python et bash qui sont utilisés pour l'analyse des données avant de les ajouter au Jbrowse 2.

Le dossier ``docs/`` contient les fichiers dans lesquels est écrite la documentation technique que vous êtes en train de lire, en langage reStructuredText.

Et enfin, le dossier ``src_to_follow/`` contient des liens physiques vers des scripts ou du code source du Jbrowse 2. Les fichiers *formatting_gff3_and_generating_index.sh*, *Snakefile_FREAT_haplotigs* et *Snakefile_GREAT_pseudomolecules* sont des fichiers associés au pipeline GREAT, et le fichier :ref:`package-core-util-jexl.ts <jexl_functions>` est un module du Jbrowse 2 dans lequel ont été définies des fonctions en jexl pour personnaliser certaines tracks.