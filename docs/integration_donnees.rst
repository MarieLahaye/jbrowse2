=======================
Intégration des données
=======================
Dans la suite de cette documentation, nous prendrons l'exemple des données de Riesling sur la version de développement du Jbrowse 2.

Pour pouvoir utiliser l'outil **Jbrowse CLI**, il faut exporter le chemin de NodeJS, soit à chaque lancement d'une nouvelle session ou en rajoutant la commande suivante au ``.bashrc`` :

.. code-block:: bash

	export PATH=/cm/shared/apps/node-v14.18.0/:$PATH

Les données sont stockées dans le dossier ``data/`` de la version de développement, dans le dossier ``public/``. Pour éviter de dupliquer les données, dans le dossier ``build/`` contenant la version de production, on peut tout simplement créer un lien symbolique vers ce dossier.

**Dossier data** : ``/data2/malahaye/jbrowse-components/products/jbrowse-web/public/data/``

.. _Jbrowse pseudomolécules:

Jbrowse des pseudomolécules
----------------------------
Les données associées au Jbrowse des pseudomolécules sont stockées dans le dossier : ``data/Riesling/pseudomolecules/`` ou ``data/Gewurztraminer/pseudomolecules/``, en fonction de l'organisme.

.. _Séquence de réference pseudomolécules:

0. Reference Sequence (FASTA)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
**Dossier** : ``/data2/malahaye/jbrowse-components/products/jbrowse-web/public/data/Riesling/pseudomolecules/0._Reference_Sequence/``

Tout d'abord, il faut indexer le fichier FASTA de la séquence de référence :

.. code-block:: bash

	samtools faidx Riesling_pseudomolecules_ctgP.fa

**Dossier** : ``/data2/malahaye/jbrowse-components/products/jbrowse-web/public/`` (ou ``build`` si on souhaite ajouter des tracks à la version de production)

Ensuite, on ajoute la séquence de référence au fichier de configuration ``config.json``, en se plaçant dans le dossier ``public/``, avec le script :ref:`initialization_tracks.py <initialization_tracks>` :

.. code-block:: bash

	/data2/malahaye/jbrowse-components/products/jbrowse-web/public/data/scripts/initialization_tracks.py --assemblyName "Riesling pseudomolecules" --add data/Riesling/pseudomolecules/0._Reference_Sequence/Riesling_pseudomolecules.ctgP.fa --conf config.json

.. _Annotations pseudomolécules:

1. Annotations (GFF3)
~~~~~~~~~~~~~~~~~~~~~

.. _Annotations Riesling pseudomolécules:

1.1 Riesling annotations (GFF3)
*******************************
**Dossier** : ``/data2/malahaye/jbrowse-components/products/jbrowse-web/public/data/Riesling/pseudomolecules/1._Annotations/1.1_Riesling_annotations``

Tout d'abord, il faut ajouter un attribut **Note** aux tRNA et ncRNA avec le script :ref:`add_attributes_gff3.py <add_attributes_gff3>` :

.. code-block:: bash

	/data2/malahaye/jbrowse-components/products/jbrowse-web/build/data/scripts/add_attributes_gff3.py add-note -i Riesling_annotations.ori.gff3 -o Riesling_annotations.with_notes.gff3 --deleteDB

Avec le même script, on va ensuite ajouter les annotations fonctionnelles dans un nouvel attribut **gene_description**, à partir d'un fichier annot contenant pour chaque gène une annotation :

.. code-block:: bash

	ln -s /home/malahaye/Archives/Assemblage_RI_final_2020/functional_annotation_blast2go_REF/Riesling_prot.fna.b2g.annot.annot
	/data2/malahaye/git_jbrowse2/public/data/scripts/add_attributes_gff3.py add-gene-description -i Riesling_annotations.with_notes.gff3 -o Riesling_annotations.with_notes.with_gene_description.gff3 --annot Riesling_prot.fna.b2g.annot.annot --deleteDB

Le fichier obtenu en sortie doit ensuite être trié, compressé et indexé :

.. code-block:: bash

	gt gff3 -sortlines -tidy Riesling_annotations.with_notes.with_gene_description.gff3 > Riesling_annotations.with_notes.with_gene_description.sorted.gff3
	bgzip Riesling_annotations.with_notes.with_gene_description.sorted.gff3
	tabix -p gff Riesling_annotations.with_notes.with_gene_description.sorted.gff3.gz

**Dossier** : ``/data2/malahaye/jbrowse-components/products/jbrowse-web/public/``

Création de la track des annotations :

.. code-block:: bash

	/data2/malahaye/jbrowse-components/products/jbrowse-web/public/data/scripts/initialization_tracks.py --assemblyName "Riesling pseudomolecules" --add data/Riesling/pseudomolecules/1._Annotations/1.1_Riesling_annotations/Riesling_annotations.with_notes.with_gene_description.sorted.gff3.gz --conf config.json

.. _Annotations PN12Xv2 pseudomolécules:

1.2 Annotations précédentes du génome de référence de la vigne - PN12Xv2 (GFF3)
*******************************************************************************
L'outil **Liftoff** permet de mapper les annotations d'un génome de référence, sur un autre génome. Dans notre cas, on souhaite mapper les annotations de la version précédente du génome de référence de la vigne : *CRIBIv1*, *CRIBIv2.1* et *VCost.v3*, sur les génomes de Riesling et de Gewurztraminer. Pour pouvoir l'utiliser, il faut tout d'abord charger l'outil, avec la commande :

.. code-block:: bash

	module load liftoff/v1.6.1

Cet outil nécessite 3 fichiers en entrée :
	- le génome de référence : *PN12Xv2.fa*
	- le génome "cible" : FASTA de Riesling ou de Gewurztraminer
	- les annotations de référence : *Total_cribi_V1.gff3*, *Total_cribi_V2.1.gff3* ou *VCost.v3_20.formatted.gff3*

**Dossier** : ``/data2/malahaye/jbrowse-components/products/jbrowse-web/public/data/Riesling/pseudomolecules/1._Annotations/1.2_PN12Xv2_annotations/``. Tous les fichiers de PN12Xv2 qui seront utilisés pour le liftoff sont retrouvés dans le dossier ``PN12X_files/`` de ce répertoire.

.. _CRIBI V1 pseudomolécules:

CRIBI V1
^^^^^^^^
**Dossier** : ``/data2/malahaye/jbrowse-components/products/jbrowse-web/public/data/Riesling/pseudomolecules/1._Annotations/1.2_PN12Xv2_annotations/``

Pour mapper les annotations CRIBI V1, nous allons utiliser notamment les fichiers suivants (pour lesquels sont indiqués les dossiers dont ils proviennent) :
	- *Total_CRIBI_V1.gff3* : ``~/Archives/Data/PN12Xv2/CRIBI_V1_PN12Xv2/``
	- *PN12Xv2_chloro_mito.for_CRIBI.fa* : ``~/Archives/Data/PN12Xv2/``

.. code-block:: bash

	liftoff -g PN12X_files/Total_CRIBI_V1.gff3 -o CRIBI_V1.liftoff.gff3 /data2/malahaye/jbrowse-components/products/jbrowse-web/public/data/Riesling/pseudomolecules/0._Reference_Sequence/Riesling_pseudomolecules.ctgP.fa PN12X_files/PN12Xv2_chloro_mito.for_CRIBI.fa

Il faut ensuite ajouter l'attribut **Name** (s'il n'y en a pas dans le fichier) à partir de l'ID, afin que le Jbrowse2 puisse afficher le nom du gène. Pour cela, on peut utiliser le script :ref:`add_attributes_gff3.py <add_attributes_gff3>` :

.. code-block:: bash

	/data2/malahaye/jbrowse-components/products/jbrowse-web/public/data/scripts/add_attributes_gff3.py add-name -i CRIBI_V1.liftoff.gff3 -o CRIBI_V1.liftoff.with_names.gff3 --deleteDB

Tri, compression et indexation :

.. code-block:: bash

	gt gff3 -sortlines -tidy CRIBI_V1.liftoff.with_names.gff3 > CRIBI_V1.liftoff.with_names.sorted.gff3
	bgzip CRIBI_V1.liftoff.with_names.sorted.gff3
	tabix -p gff CRIBI_V1.liftoff.with_names.sorted.gff3.gz

**Dossier** : ``/data2/malahaye/jbrowse-components/products/jbrowse-web/public/``

Création de la track :

.. code-block:: bash

	/data2/malahaye/jbrowse-components/products/jbrowse-web/public/data/scripts/initialization_tracks.py --assemblyName "Riesling pseudomolecules" --add data/Riesling/pseudomolecules/1._Annotations/1.2_PN12Xv2_annotations/CRIBI_V1.liftoff.with_names.sorted.gff3.gz --conf config.json

.. _CRIBI V21 pseudomolécules:

CRIBI V2.1
^^^^^^^^^^
**Dossier** : ``/data2/malahaye/jbrowse-components/products/jbrowse-web/public/data/Riesling/pseudomolecules/1._Annotations/1.2_PN12Xv2_annotations/``

Ensuite, les commandes sont identiques pour mapper les annotations CRIBI V2.1, uniquement le fichier d'annotations de référence change : *Total_CRIBI_V2.1_PN12Xv2.gff3*, venant du dossier ``~/Archives/Data/PN12Xv2/CRIBI_V2.1_PN12Xv2/``.

Liftoff :

.. code-block:: bash

	liftoff -g PN12X_files/Total_CRIBI_V2.1_PN12Xv2.gff3 -o CRIBI_V2_1.liftoff.gff3 /data2/malahaye/jbrowse-components/products/jbrowse-web/public/data/Riesling/pseudomolecules/0._Reference_Sequence/Riesling_pseudomolecules.ctgP.fa PN12X_files/PN12Xv2_chloro_mito.for_CRIBI.fa

Ajout de l'attribut **Name** sur chaque ligne avec le script :ref:`add_attributes_gff3.py <add_attributes_gff3>` :

.. code-block:: bash

	/data2/malahaye/jbrowse-components/products/jbrowse-web/public/data/scripts/add_attributes_gff3.py add-name -i CRIBI_V2_1.liftoff.gff3 -o CRIBI_V2_1.liftoff.with_names.gff3 --deleteDB

Tri, compression, indexation :

.. code-block:: bash

	gt gff3 -sortlines -tidy CRIBI_V2_1.liftoff.with_names.gff3 > CRIBI_V2_1.liftoff.with_names.sorted.gff3
	bgzip CRIBI_V2_1.liftoff.with_names.sorted.gff3
	tabix -p gff CRIBI_V2_1.liftoff.with_names.sorted.gff3.gz

**Dossier** : ``/data2/malahaye/jbrowse-components/products/jbrowse-web/public/``

Création de la track :

.. code-block:: bash

	/data2/malahaye/jbrowse-components/products/jbrowse-web/public/data/scripts/initialization_tracks.py --assemblyName "Riesling pseudomolecules" --add data/Riesling/pseudomolecules/1._Annotations/1.2_PN12Xv2_annotations/CRIBI_V2_1.liftoff.with_names.sorted.gff3.gz --conf config.json

.. _VCOSTv3 pseudomolécules:

VCOSTv3
^^^^^^^
**Dossier** : ``/data2/malahaye/jbrowse-components/products/jbrowse-web/public/data/Riesling/pseudomolecules/1._Annotations/1.2_PN12Xv2_annotations/``

Enfin, pour mapper les annotations de VCOSTv3, la méthode est quasiment identique. En revanche, le fichier est moins bien formaté. Tout d'abord, il faut supprimer à la main les "," en trop dans le fichier (c'est à dire placés juste avant un ";"). Pour cela, il vaut mieux penser à faire une copie du ficher d'origine pour garder une version sans modifications. On peut appeler la copie ``PN12X_files/VCost.v3_20.formatted.ori.gff3`` par exemple, qui sera le fichier d'origine sans modification. Tandis que le fichier qui sera modifié sera nommé ``VCost.v3_20.formatted.gff3``.

Certains subfeatures tels que des exons, des UTR et des CDS sont dupliqués et ne présentent pas les mêmes valeurs d'attribut **Parent**. Pour complètement éviter les problèmes de parents multiples ou de subfeatures dupliqués, on peut convertir le fichier GFF3 au format GTF avec la commande : 

.. code-block:: bash

	/cm/shared/apps/gffread-0.11.5.Linux_x86_64/gffread VCost.v3_20.formatted.gff3 -T -o VCost.v3_20.formatted.gtf

C'est ce fichier bien formaté qu'il faut utiliser pour réaliser le liftoff. Et contrairement aux annotations *CRIBI*, cette fois-ci, il faut utiliser la séquence de référence contenue dans le fichier *PN12Xv2.for_VCost.fa* :

.. code-block:: bash

	liftoff -f features.txt -g PN12X_files/VCost.v3_20.formatted.gtf -o VCOSTv3.liftoff.gtf /data2/malahaye/jbrowse-components/products/jbrowse-web/public/data/Riesling/pseudomolecules/0._Reference_Sequence/Riesling_pseudomolecules.ctgP.fa PN12X_files/PN12Xv2.for_VCost.fa

*Note* : Dans le fichier *features.txt* il faut indiquer la liste des types de features à analyser (un type par ligne) : ici, **transcript** et **exon**.

Le Jbrowse 2 n'acceptant que les fichiers GFF3, il faut convertir le fichier GTF de sortie du liftoff, au format GFF3 :

.. code-block:: bash

	/cm/shared/apps/gffread-0.11.5.Linux_x86_64/gffread -E VCOSTv3.liftoff.gtf -o VCOSTv3.liftoff.gff3

Ce fichier est très simplifié, et les features n'ont pas beaucoup d'attributs. Comme pour les annotations CRIBI, il faut ajouter au moins l'attribut **Name**, pour pouvoir afficher les noms des transcrits dans le Jbrowse2, avec le script :ref:`add_attributes_gff3.py <add_attributes_gff3>` :

.. code-block:: bash

	/data2/malahaye/jbrowse-components/products/jbrowse-web/public/data/scripts/add_attributes_gff3.py add-name -i VCOSTv3.liftoff.gff3 -o VCOSTv3.liftoff.with_names.gff3 --deleteDB

Tri, compression et indexation :

.. code-block:: bash

	gt gff3 -sortlines -tidy VCOSTv3.liftoff.with_names.gff3 > VCOSTv3.liftoff.with_names.sorted.gff3
	bgzip VCOSTv3.liftoff.with_names.sorted.gff3
	tabix -p gff VCOSTv3.liftoff.with_names.sorted.gff3.gz

**Dossier** : ``/data2/malahaye/jbrowse-components/products/jbrowse-web/public/``

Création de la track :

.. code-block:: bash

	/data2/malahaye/jbrowse-components/products/jbrowse-web/public/data/scripts/initialization_tracks.py --assemblyName "Riesling pseudomolecules" --add data/Riesling/pseudomolecules/1._Annotations/1.2_PN12Xv2_annotations/VCOSTv3.liftoff.with_names.sorted.gff3.gz --conf config.json

.. _Indexation des features pseudomolécules:

Indexation des features
***********************
**Dossier** : ``/data2/malahaye/jbrowse-components/products/jbrowse-web/public/``

Les features sont ensuite indexées selon leur Name, ID, avec la commande **text-index**, afin de pouvoir les rechercher en entrant le nom d'une feature :

.. code-block:: bash

	jbrowse text-index --attributes=Name,ID

.. _RNA-Seq pseudomolécules:

2. RNA-Seq
~~~~~~~~~~

.. _Alignments pseudomolécules

2.1 Alignments (BAM)
********************

.. _Préparation analyse GREAT pseudomolécules

Préparation de l'analyse
^^^^^^^^^^^^^^^^^^^^^^^^
Les fichiers BAM sont générés avec le pipeline GREAT, qui aligne les données de séquençage RNA-Seq sur la séquence de référence, avec l'outil **STAR**. Avant de lancer le pipeline, il faut indiquer le chemin vers les données de séquençage, les annotations de la séquence de référence, ainsi que le dossier dans lequel se trouvent les indexs du génome, dans le fichier ``GREAT_config.json``. Ensuite, pour cette analyse, il faut ajouter 2 options (*--alignMatesGapMax 20000 --alignIntronMax 20000*) dans la commande STAR, afin de filtrer les paires de reads ou deux portions d'un même read dont la distance les séparant est supérieure à 20000 bases :

.. code-block:: bash

	shell("touch {log}; STAR --alignMatesGapMax 20000 --alignIntronMax 20000 --outMultimapperOrder Random --outSAMunmapped Within --readFilesCommand zcat --runThreadN {threads} --outFilterMismatchNmax 999 --outSAMprimaryFlag OneBestScore --outSAMattributes All --genomeDir {input.genome_dir} --readFilesIn {fastq_name}_1.fastq.gz {fastq_name}_2.fastq.gz --outFileNamePrefix {output}_ 2> {log}; touch {output}")

.. _Analyse GREAT pseudomolécules

Analyse avec le pipeline GREAT
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Le pipeline GREAT peut ensuite être lancé avec la commande suivante (sur le noeud 001 avec 24 CPU) :

.. code-block:: bash

	sbatch -c 24 -w node001 /data2/malahaye/data_jbrowse/Riesling/RNAseq_data/analyse_RNAseq_RI/GREAT_run.sh

Les fichiers BAM qui vont être ensuite intégrés au Jbrowse2 sont stockés dans le dossier ``/data2/malahaye/data_jbrowse/Riesling/RNAseq_data/analyse_RNAseq_RI/STAR_PARSED/``.

.. _Track Alignments pseudomolécules

Création de la track des données d'alignements
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
On peut ensuite déplacer tous les fichiers BAM dans le dossier qui va stocker les données RNA-Seq du Jbrowse2 des haplotigs :

.. code-block:: bash

	mv /data2/malahaye/data_jbrowse/Riesling/RNAseq_data/analyse_RNAseq_RI/STAR_PARSED/*/*.bam /data2/malahaye/jbrowse-components/products/jbrowse-web/build/data/Riesling/pseudomolecules/2._RNA-Seq/2.1_Alignments/

**Dossier** : ``/data2/malahaye/jbrowse-components/products/jbrowse-web/build/data/Riesling/pseudomolecules/2._RNA-Seq/2.1_Alignments/``

ndexation des fichiers BAM :

.. code-block:: bash

	for file in *bam
	do
		echo "Indexing ${file}..."
		samtools index ${file}
	done

**Dossier** : ``/data2/malahaye/jbrowse-components/products/jbrowse-web/public/``

Enfin, les tracks sont créées avec le script ``bamtracks.sh`` pour l'ensemble des fichiers BAM indexés précédemment :

.. code-block:: bash

	/data2/malahaye/jbrowse-components/products/jbrowse-web/public/data/scripts/initialization_tracks.py --assemblyName "Riesling pseudomolecules" --add data/Riesling/pseudomolecules/2._RNA-Seq/2.1_Alignments/*.bam --conf config.json --metadata data/Riesling/pseudomolecules/RI_metadata.json

.. _BigWig pseudomolécules:

2.2 BigWig (BW)
***************
**Dossier** : ``/data2/malahaye/jbrowse-components/products/jbrowse-web/build/data/Riesling/pseudomolecules/2._RNA-Seq/``

La densité de reads alignés sur chaque portion du génome (RNAseq ici) peut être représentée sous forme d’un graph XY ou sous la forme de variation d’intensité de couleur. Ces tracks vont donner une information sur l'expression des gènes. Les fichiers BigWig sont générés à partir des fichiers WIG qui eux sont générés à partir des fichiers d’alignements BAM, avec le script ``bamtobigwigNormalized.sh``, qui prend en argument *Riesling* ou *Gewurztraminer* :

.. code-block:: bash

	/data2/malahaye/jbrowse-components/products/jbrowse-web/build/data/scripts/bamtobigwigNormalized.sh -i `ls 2.1_Alignments/*.bam | tr "\n" ","` -t /home/malahaye/data2/ -o 2.2_BigWig_XY/ 

Ensuite, les tracks correspondant aux fichiers BigWig générés peuvent être créées avec le script ``bigwigtracks.sh``:

.. code-block:: bash

	/data2/malahaye/jbrowse-components/products/jbrowse-web/public/data/scripts/initialization_tracks.py --assemblyName "Riesling pseudomolecules" --add data/Riesling/pseudomolecules/2._RNA-Seq/2.2_BigWig_XY/*.bw --conf config.json --metadata data/Riesling/pseudomolecules/RI_metadata.json

.. _Sashimi plot pseudomolécules:

Sashimi plot (BED)
******************
**Dossier** : ``/data2/malahaye/jbrowse-components/products/jbrowse-web/build/data/Riesling/pseudomolecules/2._RNA-Seq/``

Pour représenter les sashimi plots des données RNA-Seq, il faut générer un fichier BED contenant les jonctions entre les exons :

.. code-block:: bash

	/data2/malahaye/jbrowse-components/products/jbrowse-web/build/data/scripts/bedsashimi.sh -i `ls 2.1_Alignments/*.bam | tr "\n" ","` -o 2.3_Sashimi_plot/

Création des tracks correspondant aux différents fichiers générés :

.. code-block:: bash

	/data2/malahaye/jbrowse-components/products/jbrowse-web/build/data/scripts/initialization_tracks.py --assemblyName "Riesling pseudomolecules" --add data/Riesling/pseudomolecules/2._RNA-Seq/2.3_Sashimi_plot/*.bed.gz --conf config.json --metadata data/Riesling/pseudomolecules/RI_metadata.json

.. _Données RNAseq mergées pseudomolécules:

Données RNAseq mergées
**********************

.. _Fichiers alignements mergés pseudomolécules:

Fichiers d'alignements (BAM)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
**Dossier** : ``/data2/malahaye/jbrowse-components/products/jbrowse-web/build/data/Riesling/pseudomolecules/2._RNA-Seq/merged_RNAseq/``

Il faut tout d'abord charger 2 modules :

.. code-block:: bash

	module load jdk/11.0.6
	module load samtools/1.9

Les fichiers BAM sont ensuite mergés avec la commande suivante. L'option **-@** permet d'indiquer le nombre de threads sur lequel on souhaite lancer l'analyse :

.. code-block:: bash

	samtools merge -@ 5 merged.bam /data2/malahaye/jbrowse-components/products/jbrowse-web/build/data/Riesling/pseudomolecules/2._RNA-Seq/2.1_Alignments/*.bam

Ensuite, le fichier des BAM mergés doit être trié en fonction du nom de la référence, et pas en fonction des coordonnées :

.. code-block:: bash

	java -jar /cm/shared/apps/jvarkit/dist/sortsamrefname.jar --maxRecordsInRam 5000000 \
	--tmpDir . \
	--samoutputformat BAM -o merged.sortedbyname.bam merged.bam

Le fichier mergé étant très lourd, on va réduire la profondeur à 250 :

.. code-block:: bash

	java -jar /cm/shared/apps/jvarkit/dist/biostar154220.jar -n 250 --samoutputformat BAM \
	-o merged.depth_max_250.bam merged.sortedbyname.bam

Il faut ensuite, à nouveau, trier le fichier BAM avec la commande, cette fois en fonction des coordonnées :

.. code-block:: bash

	samtools sort -@ 5 -o merged.depth_max_250.sorted.bam -T  . merged.depth_max_250.bam

Pour finir, on peut déplacer le fichier BAM dans le dossier correspondant aux données d'alignements ``2.1_Alignments/``, puis l'indexer :

.. code-block:: bash

	samtools index merged.depth_max_250.sorted.bam

Création de la track des BAM mergés :

.. code-block:: bash

	/data2/malahaye/jbrowse-components/products/jbrowse-web/public/data/scripts/initialization_tracks.py --assemblyName "Riesling pseudomolecules" --add data/Riesling/pseudomolecules/2._RNA-Seq/2.1_Alignments/merged.depth_max_250.sorted.bam --conf config.json

.. _Densité alignements mergés pseudomolécules:

Données sur la densité des alignements (BigWig)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Tout d'abord, il faut charger le module **WigToBigWig**, et faire un lien symbolique (par exemple) vers le fichier ``RI.chrom.sizes`` contenant la taille des chromosomes :

.. code-block:: bash

	module load wigToBigWig/1.0

Ensuite, il faut générer le fichier WIG à partir du fichier BAM des données mergées, puis le fichier BigWig à partir du fichier WIG obtenu à l'étape précédente, en se plaçant dans le dossier dans lequel se trouvent les données mergées :

.. code-block:: bash

	bam2wig -t merged merged.depth_max_250.sorted.bam > merged.depth_max_250.sorted.wig
	wigToBigWig merged.depth_max_250.sorted.wig /data2/malahaye/jbrowse-components/products/jbrowse-web/public/data/Riesling/pseudomolecules/RI.chrom.sizes merged.depth_max_250.sorted.bw

Après avoir déplacé le fichier BigWig dans le dossier ``2.2_BigWig_XY/``, création de la track permettant de représenter les informations contenues dans le fichier BigWig :

.. code-block:: bash

	/data2/malahaye/jbrowse-components/products/jbrowse-web/public/data/scripts/initialization_tracks.py --assemblyName "Riesling pseudomolecules" --add data/Riesling/pseudomolecules/2._RNA-Seq/2.2_BigWig_XY/merged.depth_max_250.sorted.bw --conf config.json

*Note* : Le fichier contenant la taille des chromosomes peut être généré avec la commande :

.. code-block:: bash

	cut -f1,2 /data2/malahaye/jbrowse-components/products/jbrowse-web/public/data/Riesling/pseudomolecules/0._Reference_Sequence/Riesling_pseudomolecules.ctgP.fa.fai > RI.chrom.sizes

.. _Sashimi plot mergés pseudomolécules:

Sashimi Plot (BED)
^^^^^^^^^^^^^^^^^^
Dossier : ``/data2/malahaye/jbrowse-components/products/jbrowse-web/build/data/Riesling/pseudomolecules/2._RNA-Seq/``

Pour représenter les sashimi plots, il faut générer un fichier BED contenant les jonctions entre les exons, à partir du fichier BAM mergé :

.. code-block:: bash

	/data2/malahaye/jbrowse-components/products/jbrowse-web/build/data/scripts/bedsashimi.sh -i 2.1_Alignments/merged.depth_max_250.sorted.bam -o 2.3_Sashimi_plot/

Création de la track :

.. code-block:: bash

	/data2/malahaye/jbrowse-components/products/jbrowse-web/public/data/scripts/initialization_tracks.py --assemblyName "Riesling pseudomolecules" --add data/Riesling/pseudomolecules/2._RNA-Seq/2.3_Sashimi_plot/*.bed.gz  --conf config.json

.. _Données reséquençage pseudomolécules:

Données de reséquençage (BAM)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
**Dossier** : ``/data2/malahaye/jbrowse-components/products/jbrowse-web/build/data/Riesling/pseudomolecules/3._WGS/resequencing/``

Pour aller plus rapidement, il est possible de lancer l'alignement des données de reséquençage directement sur le FASTA mergé des pseudomolécules et des haplotigs. La première étape est d'indexer le FASTA :

.. code-block:: bash

	cat  Riesling_haplotigs.fa Riesling_pseudomolecules_ctgP.fa > Riesling_pseudomolecules_haplotigs_merged.fa
	/cm/shared/apps/bwa-mem2/bwa-mem2 index -p Riesling_pseudomolecules_haplotigs_merged Riesling_pseudomolecules_haplotigs_merged.fa

Ensuite, les données de reséquençage peuvent être alignées sur les séquences des pseudomolécules et les haplotigs :

.. code-block:: bash

	srun -c 12 -w node001 /cm/shared/apps/bwa-mem2/bwa-mem2 mem -o Riesling_resequencing_data_pseudomolecules_haplotigs.sam -t 12 -a -R '@RG\tID:Riesling\tSM:Riesling' Riesling_pseudomolecules_haplotigs_merged ~/Archives/SeRiGe_201406041640/Reads/Riesling_raw/Riesling_1.fastq.gz ~/Archives/SeRiGe_201406041640/Reads/Riesling_raw/Riesling_2.fastq.gz 2> bwa-mem2_mem.log &

Le fichier SAM ainsi obtenu doit être converti en fichier BAM (il est important de ne pas filtrer, à cette étape, les reads multimappés), puis trié :

.. code-block:: bash

	srun -c 1 -w node001 /cm/shared/apps/samtools-1.9/samtools-1.9/bin/samtools view -bS Riesling_resequencing_data_pseudomolecules_haplotigs.sam -o Riesling_resequencing_data_pseudomolecules_haplotigs.bam 2> samtools_view.log &
	srun -c 12 -w node001 /cm/shared/apps/samtools-1.9/samtools-1.9/bin/samtools sort -@ 12 Riesling_resequencing_data_pseudomolecules_haplotigs.bam -o Riesling_resequencing_data_pseudomolecules_haplotigs.sorted.bam 2> samtool-sort.log &

Pour finir, il faut filtrer le fichier BAM pour récupérer uniquement les reads alignés sur les pseudomolécules. Pour cela, le BAM doit être tout d'abord indexé :

.. code-block:: bash

	samtools index Riesling_resequencing_data_pseudomolecules_haplotigs.sorted.bam
	/data2/malahaye/jbrowse-components/products/jbrowse-web/build/data/scripts/filterbam.sh -i Riesling_resequencing_data_pseudomolecules_haplotigs.sorted.bam -f /data2/malahaye/jbrowse-components/products/jbrowse-web/build/data/Riesling/pseudomolecules/0._Reference_Sequence/Riesling_pseudomolecules.ctgP.fa -o .

On peut alors déplacer le fichier filtré ``Riesling_resequencing_data_pseudomolecules_haplotigs.sorted.filtered.bam`` dans le dossier ``3._WGS/`` (on peut en profiter pour le renommer), puis l'indexer :

.. code-block:: bash

	samtools index Riesling_resequencing_data.pseudomolecules.sorted.filtered.bam

Création de la track :

.. code-block:: bash

	/data2/malahaye/jbrowse-components/products/jbrowse-web/public/data/scripts/initialization_tracks.py --assemblyName "Riesling pseudomolecules" --add data/Riesling/pseudomolecules/3._WGS/Riesling_resequencing_data.pseudomolecules.sorted.filtered.bam  --conf config.json

.. _Séquences manquantes pseudomolécules:

Séquences manquantes (BED)
~~~~~~~~~~~~~~~~~~~~~~~~~~
Chargement de l'environnement conda sur lequel est installé l'outil **bedToBigBed** :

.. code-block:: bash

	module load python/3.8

Pour récupérer les régions du génome où se trouvent des Ns. L’outil seqtk permet, à partir du fichier FASTA de la séquence de référence, de générer un fichier BED contenant toutes les coordonnées des portions de séquences représentées par un ou plusieurs Ns. Après s’être placé dans le dossier contenant le fichier FASTA :

.. code-block:: bash

	seqtk cutN -n 1 -p 10000 -g Riesling_pseudomolecules_ctgP.fa > Riesling_Ngaps_without_name.bed
	/data2/malahaye/jbrowse-components/products/jbrowse-web/public/data/scripts/add_name_bed.py -i Riesling_Ngaps_without_name.bed -o Riesling_Ngaps.bed

Tri du fichier BED, puis création du fichier BigBed, qui est un fichier binaire indexé :

.. code-block:: bash

	sortBed -i Riesling_Ngaps.bed > Riesling_Ngaps.sorted.bed
	bedToBigBed Riesling_Ngaps.sorted.bed RI.chrom.sizes Riesling_Ngaps.sorted.bb

Création de la track des séquences manquantes :

.. code-block:: bash

	/data2/malahaye/jbrowse-components/products/jbrowse-web/public/data/scripts/initialization_tracks.py --assemblyName "Riesling pseudomolecules" --add data/Riesling/pseudomolecules/5._Others/N_gaps.sorted.bb --conf config.json

.. _Contigs primaires pseudomolécules:

Contigs primaires (BED)
~~~~~~~~~~~~~~~~~~~~~~~
La liste des contigs primaires et de leur taille est contenue dans le fichier NewNames_OperaFormat.txt (à partir duquel peut être généré un fichier BED contenant les start et end des contigs primaires :

.. code-block:: bash

	/data2/malahaye/jbrowse-components/products/jbrowse-web/public/data/scripts/bed_contigs.py -i NewNames_OperaFormat.txt -o contigs.bed

Ce fichier BED ainsi créé, doit ensuite être trié, pour pouvoir générer le fichier BigBed associé :

.. code-block:: bash

	sortBed -i contigs.bed > contigs.sorted.bed
	bedToBigBed contigs.sorted.bed RI.chrom.sizes contigs.sorted.bb

Création de la track des contigs primaires :

.. code-block:: bash

	/data2/malahaye/jbrowse-components/products/jbrowse-web/public/data/scripts/initialization_tracks.py --assemblyName "Riesling pseudomolecules" --add data/Riesling/pseudomolecules/5._Others/Contigs.sorted.bb --conf config.json

.. _Haplotigs pseudomolécules:

Haplotigs (BED)
~~~~~~~~~~~~~~~
Génération du fichier BED contenant les positions des haplotigs par rapport à la séquence de référence et le pourcentage d’identité des alignements :

.. code-block:: bash

	/data2/malahaye/jbrowse-components/products/jbrowse-web/public/data/scripts/bedhaplotigs.sh

Pour créer les tracks des haplotigs et des allèles, il n'est pas possible d'utiliser des fichiers BigBed, car ceux-ci n'acceptent pas les nombres "flottants", ainsi que les "." au niveau du score. Donc, comme pour le Jbrowse, il faut trier, compresser, puis indexer le fichier BED avant de pouvoir créer les tracks correspondantes :

.. code-block:: bash

	sortBed -i haplotigs.bed > haplotigs.sorted.bed
	bgzip haplotigs.sorted.bed
	tabix -p bed haplotigs.sorted.bed.gz

Création de la track des haplotigs :

.. code-block:: bash

	cp /data2/malahaye/jbrowse-components/products/jbrowse-web/public/data//Riesling/pseudomolecules/4._Haplotigs/alignments_haplotigs/haplotigs.sorted.bed.gz /data2/malahaye/jbrowse-components/products/jbrowse-web/public/data//Riesling/pseudomolecules/4._Haplotigs/Alleles.sorted.bed.gz
	cp /data2/malahaye/jbrowse-components/products/jbrowse-web/public/data//Riesling/pseudomolecules/4._Haplotigs/alignments_haplotigs/haplotigs.sorted.bed.gz.tbi /data2/malahaye/jbrowse-components/products/jbrowse-web/public/data//Riesling/pseudomolecules/4._Haplotigs/Alleles.sorted.bed.gz.tbi
	/data2/malahaye/jbrowse-components/products/jbrowse-web/public/data/scripts/initialization_tracks.py --assemblyName "Riesling pseudomolecules" --add data/Riesling/pseudomolecules/4._Haplotigs/Haplotigs.sorted.bed.gz --conf config.json

.. _Allèles pseudomolécules:

Alleles (BED)
~~~~~~~~~~~~~
Pour déterminer quels gènes présentent un allèle sur les haplotigs, il faut aligner les CDS alternatifs sur les CDS de référence, filtrer les alignements et enfin, générer un fichier BED contenant les coordonnées des allèles sur la pseudomolécule. Toutes les commandes sont dans le script ``bedcdsalt.sh`` :

.. code-block:: bash

	/data2/malahaye/jbrowse-components/products/jbrowse-web/public/data/scripts/bedcdsalt.sh

Tri, compression et indexation :

.. code-block:: bash

	sortBed -i cds_alt.bed > cds_alt.sorted.bed
	bgzip cds_alt.sorted.bed
	tabix -p bed cds_alt.sorted.bed.gz

Copie des fichiers dans le dossier ``4._Haplotigs``, pour lui donner le nom que l'on souhaite donner à la track (il faut le mettre directement dans le dossier dont le nom correspond à la catégorie à laquelle elle appartient) :

.. code-block:: bash

	cd /data2/malahaye/jbrowse-components/products/jbrowse-web/public/data/Riesling/pseudomolecules/4._Haplotigs/
	cp alignments_cds/cds_alt.sorted.bed.gz Alleles.sorted.bed.gz
	cp alignments_cds/cds_alt.sorted.bed.gz.tbi Alleles.sorted.bed.gz.tbi

Création de la track des allèles :

.. code-block:: bash

	cd /data2/malahaye/jbrowse-components/products/jbrowse-web/public/
	/data2/malahaye/jbrowse-components/products/jbrowse-web/public/data/scripts/initialization_tracks.py --assemblyName "Riesling pseudomolecules" --add data/Riesling/pseudomolecules/4._Haplotigs/Alleles.sorted.bed.gz --conf config.json

.. _Correspondances ref alt pseudomolécules:

Indication correspondances gene ID ref et alt (GFF)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Maintenant, il faut modifier le fichier d'annotations des haplotigs pour ajouter un ou des attributs 'id_ref', indiquant les identifiants des gènes des pseudomolécules correspondant aux gènes des haplotigs. Les correspondances ALT/REF ont été obtenues à l'étape précédente à partir des alignements des CDS alternatifs sur les CDS de référence. Le script ``bed_cds_alt.py``, en plus de générer un fichier BED, génère automatiquement un fichier ``genes_list.tsv`` au format tabulé, contenant les informations suivantes : ID du CDS alternatif, ID du CDS de référence, 'haplotig=partiel' (dans le cas où le CDS dépasse de l'haplotig sur lequel il est localisé).  Le script ``formatting_fna_gff.py`` permet notamment d'ajouter des attributs 'id_ref' en se basant sur ce fichier tabulé :

.. code-block:: bash

	cd /data2/malahaye/data_jbrowse/Riesling/alt_data_with_correspondances/
	/data2/malahaye/git_jbrowse2/public/data/scripts/formatting_fna_gff.py add-attribute -i ori_files/sequences.gff3 -g genes_list.tsv -o sequences.final.gff3

*Note* : Le fichier de sortie contient moins de lignes car la librairie utilisée en python pour parcourir le GFF3 ne conserve pas les lignes commençant par "###".

.. _Jbrowse hplotigs:

Jbrowse des haplotigs
---------------------

.. _Séquences haplotigs:

Séquences des haplotigs (FASTA)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
**Dossier** :  ``/data2/malahaye/jbrowse-components/products/jbrowse-web/public/data/Riesling/haplotigs/0._Reference_Sequence``

Pour créer un nouvel assemblage pour les haplotigs, il suffit d'ajouter la séquence des haplotigs au Jbrowse2 et de lui donner un nom d'assemblage qui permettra d'y associer d'autres tracks. Tout d'abord il faut indexer le fichier FASTA :

.. code-block:: bash

	samtools faidx Riesling_haplotigs.fa

Quand le fichier est indexé, on peut ensuite ajouter l'assemblage au Jbrowse2 :

.. code-block:: bash

	/data2/malahaye/jbrowse-components/products/jbrowse-web/public/data/scripts/initialization_tracks.py --assemblyName "Riesling haplotigs" --add data/Riesling/haplotigs/0._Reference_Sequence/Riesling_haplotigs.filtered.fa --conf config.json

.. _Annotations des gènes haplotigs:

Annotations des gènes (GFF3)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
**Dossier** : ``/data2/malahaye/jbrowse-components/products/jbrowse-web/public/data/Riesling/haplotigs/1._Annotations/1.1_Riesling_annotations/``

Les annotations à ajouter sont celles contenues dans le fichier GFF3 *sequences.final.gff3* des haplotigs qui a été modifié pour ajouter l'attribut *id_ref*. Comme pour les annotations des pseudomolécules, il faut ajouter l'attribut 'Note' aux tRNA et aux ncRNA :

.. code-block:: bash

	/data2/malahaye/jbrowse-components/products/jbrowse-web/build/data/scripts/add_attributes_gff3.py add-note -i Riesling_haplotigs_annotations.with_id_ref.gff3 -o Riesling_haplotigs_annotations.with_id_ref.with_note.gff3 --deleteDB

Ensuite on peut trier, compresser et indexer le fichier :

.. code-block:: bash

	gt gff3 -sortlines -tidy -retainids Riesling_haplotigs_annotations.with_id_ref.with_note.gff3 > Riesling_haplotigs_annotations.with_id_ref.with_note.sorted.gff3
	bgzip Riesling_haplotigs_annotations.with_id_ref.with_note.sorted.gff3
	tabix -p gff Riesling_haplotigs_annotations.with_id_ref.with_note.sorted.gff3.gz

Création de la track des annotations des haplotigs :

.. code-block:: bash

	/data2/malahaye/jbrowse-components/products/jbrowse-web/public/data/scripts/initialization_tracks.py --assemblyName "Riesling pseudomolecules" --add data/Riesling/haplotigs/1._Annotations/1.1_Riesling_annotations/Riesling_haplotigs_annotations.with_id_ref.with_note.sorted.gff3.gz --conf config.json

.. _Annotations PN12Xv2 haplotigs:

Annotations précédentes de PN12v2 (GFF3)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Les commandes sont similaires à celles utilisées pour mapper ces annotations sur les pseudomolécules.

.. _Alignements haplotigs:

Fichiers d'alignements (BAM)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Préparation des données
***********************
Pour représenter les mêmes données RNA-Seq que pour les pseudomolécules, sur les haplotigs, il faudra cette fois-ci lancer le pipeline GREAT sur les pseudomolécules ET les haplotigs. Toutes les données qui seront données en entrée du pipeline sont stockées dans le dossier : ``/data2/malahaye/data_jbrowse/Riesling/RNAseq_data``. Pour commencer, il faut préparer les données, en faisant un merge des fichiers FASTA REF et ALT, et la même chose pour les fichiers GFF :

.. code-block:: bash

	cat  Riesling_haplotigs.fa Riesling_pseudomolecules_ctgP.fa > Riesling_pseudomolecules_haplotigs_merged.fa

*Note* : Il est important de mettre les fichiers dans cet ordre, dans la commande ci-dessus, sinon le fichier de sortie est mal formaté (pourquoi, c'est un mystère). 

Ensuite, il faut merger les annotations des pseudomolécules et celles des haplotigs. Il est important de faire un **grep -v "#"** pour ne pas garder les metadata commançant par "##" :

.. code-block:: bash

	ln -s /data2/malahaye/data_jbrowse/Riesling/RI_annotations.gff3 RI_pseudomolecules_annotations.gff3
	ln -s /data2/malahaye/data_jbrowse/Riesling/alt_data_with_correspondances/sequences.final.gff3 RI_haplotigs_annotations.gff3
	cat RI_pseudomolecules_annotations.gff3 RI_haplotigs_annotations.gff3 | grep -v "#" > RI_pseudomolecules_haplotigs_merged_annotations.gff3

Le script ``formatting_gff3_and_generating_index.sh`` permet de générer les index nécessaires à GREAT (il faut lancer les commandes une à une). Les principales commandes sont détaillées ci-dessous. Tout d'abord le GFF3 doit formaté pour ajouter l'attribut GeneID :

.. code-block:: bash

	/data2/malahaye/git_jbrowse2/public/data/scripts/add_attributes_gff3.py add-GeneID -i /data2/malahaye/data_jbrowse/Riesling/RNAseq_data/RI_pseudomolecules_haplotigs_merged_annotations.gff3 -o /data2/malahaye/data_jbrowse/Riesling/RNAseq_data/RI_pseudomolecules_haplotigs_merged_annotations.final.gff3 --deleteDB

**Dossier** : ``/data2/malahaye/data_jbrowse/Riesling/RNAseq_data/analyse_RNAseq_RI/``

Puis, l'outil STAR est utilisé pour générer les index sur lequel va être réalisé l'indexation. Après avoir changé d'environnement et exporté la bonne version de Perl, il peut être lancé :

.. code-block:: bash

	source activate /cm/shared/apps/miniconda3/GREAT_environment
	export PERL5LIB=/cm/shared/apps/miniconda3/lib/perl5/5.22.0/
	STAR --runThreadN 5 --runMode genomeGenerate --genomeDir index_RI --genomeFastaFiles /data2/malahaye/data_jbrowse/Riesling/RNAseq_data/Riesling_pseudomolecules_haplotigs_merged.fa --sjdbGTFfile /data2/malahaye/data_jbrowse/Riesling/RNAseq_data/RI_pseudomolecules_haplotigs_merged_annotations.final.gff3 --sjdbGTFtagExonParentTranscript Parent --sjdbGTFtagExonParentGene GeneID

Lorsque c'est fait, il faut indiquer le chemin vers les données de séquençage, les annotations de la séquence de référence, ainsi que le dossier dans lequel se trouvent les indexs du génome, dans le fichier ``GREAT_config.json``. Dans le Snakefile (qui est suivi sur GitLab), il faut enlever le filtre des alignements multiples (-F 0x100) dans la commande suivante :

.. code-block:: bash

	"samtools view -F 0x100 -bS {input.sam}_Aligned.out.sam | samtools sort -o {output}.tmp -T $( pwd ) -; bamtools filter -tag \"NM:<={params.edit_distance}\" -in {output}.tmp -out {output}; rm {output}.tmp"

Analyse avec le pipeline GREAT
******************************
Lorsque toutes les données ont été préparées, GREAT peut être lancé avec la commande suivante (ici, sur le noeud 001, avec 24 CPU) :

.. code-block:: bash

	sbatch -c 24 -w node001 /data2/malahaye/data_jbrowse/Riesling/RNAseq_data/analyse_RNAseq_RI/GREAT_run.sh

Les fichiers BAM qui vont être ensuite intégrés au Jbrowse2 sont stockés dans le dossier ``/data2/malahaye/data_jbrowse/Riesling/RNAseq_data/analyse_RNAseq_RI/STAR_PARSED/``.

Création de la track des données d'alignements
**********************************************
On peut donc déplacer tous les fichiers BAM dans le dossier qui va stocker les données RNA-Seq du Jbrowse2 des haplotigs :

.. code-block:: bash

	mv /data2/malahaye/data_jbrowse/Riesling/RNAseq_data/analyse_RNAseq_RI/STAR_PARSED/*/*.bam /data2/malahaye/jbrowse-components/products/jbrowse-web/build/data/Riesling/haplotigs/2._RNA-Seq/2.1_Alignments/ori/

Indexation des fichiers BAM, en se plaçant dans le dossier où ils sont localisés :

.. code-block:: bash

	for file in *bam
	do 
		echo "Indexing ${file}..."
		samtools index ${file}
	done

Pour ne conserver que les reads alignés sur les haplotigs, il faut filtrer ces fichiers BAM, en se basant sur les ID des haplotigs, retrouvés dans le FASTA des haplotigs (ici on se place dans le dossier ``/data2/malahaye/jbrowse-components/products/jbrowse-web/build/data/Riesling/haplotigs/2._RNA-Seq/2.1_Alignments``) :

.. code-block:: bash

	/data2/malahaye/jbrowse-components/products/jbrowse-web/build/data/scripts/filterbam.sh -i `ls ori/*.bam | tr "\n" ","` -f /data2/malahaye/jbrowse-components/products/jbrowse-web/build/data/Riesling/haplotigs/0._Reference_Sequence/Riesling_haplotigs.fa -o .

Indexation des fichiers BAM, en se plaçant dans le dossier où ils sont localisés :

.. code-block:: bash

	for file in *bam
	do 
		echo "Indexing ${file}..."
		samtools index ${file}
	done

Puis, création de la track associée à partir du dossier ``/data2/malahaye/jbrowse-components/products/jbrowse-web/public/`` : 

.. code-block:: bash

	/data2/malahaye/jbrowse-components/products/jbrowse-web/public/data/scripts/initialization_tracks.py -n "Riesling haplotigs" -c config.json -a data/Riesling/haplotigs/2._RNA-Seq/2.1_Alignments/*.bam

.. _Densité alignements haplotigs:

Représentation de la densité des alignements (BigWig)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Comme pour les pseudomolécules, il faut tout d'abord générer les fichiers BigWig en modifiant le chemin vers le dossier contenant les données, dans le script ``bamtobigwigNormalized.sh``, puis en le lançant :

.. code-block:: bash

	/data2/malahaye/jbrowse-components/products/jbrowse-web/public/data/scripts/bamtobigwigNormalized.sh Riesling

Création des tracks représentant la densité des alignements :

.. code-block:: bash

	/data2/malahaye/jbrowse-components/products/jbrowse-web/public/data/scripts/initialization_tracks.py -n "Riesling haplotigs" -c config.json -a data/Riesling/haplotigs/2._RNA-Seq/2.2_BigWig_XY/*.bw

.. _Données RNAseq mergées haplotigs:

Données RNAseq mergées
~~~~~~~~~~~~~~~~~~~~~~
Les commandes sont similaires à celles lancées sur les données des pseudomolécules, seuls les fichiers et les chemins changent.

.. _Données reséquençage haplotigs:

Données de reséquençage (BAM)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
**Dossier** : ``/data2/malahaye/jbrowse-components/products/jbrowse-web/build/data/Riesling/haplotigs/3._WGS/resequencing/``

Les données de reséquençage ont été alignées sur le FASTA mergé des pseudomolécules et des haplotigs. Avec le BAM trié qui a été généré, on peut extraire uniquement les reads qui sont alignés sur les haplotigs, en donnant en entrée du script ``filterbam.sh``, le FASTA des haplotigs. Il faut tout d'abord copier (ou créer un lien symbolique) le BAM et le fichier indexé associé, dans le dossier ``resequencing/``, puis lancer la filtration avec la commande suivante :

.. code-block:: bash

	/data2/malahaye/jbrowse-components/products/jbrowse-web/build/data/scripts/filterbam.sh -i Riesling_resequencing_data_pseudomolecules_haplotigs.sorted.bam -f /data2/malahaye/jbrowse-components/products/jbrowse-web/build/data/Riesling/haplotigs/0._Reference_Sequence/Riesling_haplotigs.fa -o .

On peut alors déplacer le fichier filtré ``Riesling_resequencing_data_pseudomolecules_haplotigs.sorted.filtered.bam`` dans le dossier ``3._WGS/`` (on en profite pour le renommer), puis l'indexer :

.. code-block:: bash

	samtools index Riesling_resequencing_data.haplotigs.sorted.filtered.bam

Création de la track :

.. code-block:: bash

	/data2/malahaye/jbrowse-components/products/jbrowse-web/public/data/scripts/initialization_tracks.py --assemblyName "Riesling haplotigs" --add data/Riesling/haplotigs/3._WGS/Riesling_resequencing_data.haplotigs.sorted.filtered.bam  --conf config.json
