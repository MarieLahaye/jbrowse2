.. jbrowse2 documentation master file, created by
   sphinx-quickstart on Tue Sep 28 11:59:52 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

=====================
Jbrowse 2 de la vigne
=====================

.. toctree::
   :maxdepth: 3
   
   introduction
   organisation_jbrowse2
   integration_donnees
   scripts