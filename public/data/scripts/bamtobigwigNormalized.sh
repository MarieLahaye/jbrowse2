#!/usr/bin/bash
# Ce script prend beaucoup de mémoire pour stocker des fichiers temporaires lors de l'analyse --> il faut changer le chemins ...
# ... où sont stockés ces fichiers
# TMPDIR="/home/malahaye/data2/"

# Fonction permettant d'afficher le help du script
function usage {
	cat <<-__EOF__
		Usage:
			./bamtobigwigNormalized.sh -i input_files -t tempdir -o outdir [-h]

		Description:
			Create normalized (RPKM) BigWig files from BAM

		Arguments:
			-i, --input List of BAM files (required)
				Files must be separated by commas
				You can use the following command: ls path/to/*.bam | tr "\n" "," 
			-t, --tempdir Temp directory where temporary files will be created
			-o, --outdir Output directory  (required)
			-h, --help

		Exemple: ./bamtobigwigNormalized.sh -i \`ls *.bam | tr "\n" ","\` -t path/to/tempdir -o path/to/outdir
		__EOF__
}

# Eval command line arguments given in input
ARGS=$(getopt -o "i:t:o:h" --long "input:,tempdir:,outdir:,help" -- $@ 2> /dev/null)

# Check if the return code of the previous command is not equal to 0 (command ...
# ... didn't work)
# >&2 send the message to standard error (stderr) instead of standard out (stdout)
if [ $? -ne 0 ]; then
	echo "Error in the argument list. Use -h or --help to display the help." >&2
	exit 1
fi

eval set -- ${ARGS}
while true
do
	case $1 in
		-i|--input)
			INPUT_FILES=$2
			shift 2
			;;
		-o|--outdir)
			OUTPUT_DIRECTORY=$2
			shift 2
			;;
		-t|--tempdir)
			TEMP_DIRECTORY=$2
			shift 2
			;;
		-h|--help)
			usage
			exit 0
			;;
		--) shift
			break
			;;
		*)	echo "Option $1 is not recognized. Use -h or --help to display the help." && \
			exit 1
			;;
	esac
done

# Check if one required parameter is missing in the given command line
if [[ ${INPUT_FILES} == "" ]] || [[ ${TEMP_DIRECTORY} == "" ]] || [[ ${OUTPUT_DIRECTORY} == "" ]]; then
	echo "Options --input, --tempdir and --outdir are required. Use -h or --help to display the help" >&2
	exit 1
fi

# Check if FASTA exist
if ! [ -d "${TEMP_DIRECTORY}" ]; then
	echo "${TEMP_DIRECTORY} doesn't exist. Create it or enter the right path and directory name" >&2
	exit 1
fi

# Check if directory exists
if ! [ -d "${OUTPUT_DIRECTORY}" ]; then
    echo "${OUTPUT_DIRECTORY} doesn't exist. Create it or enter the right path and directory name" >&2
    exit 1
fi

for i in $(echo ${INPUT_FILES} | tr "," "\n")
do
	if [ -f $i ]; then
		file=${i##*/}
		file_name=${file%%.*}
		# normalisation avec le RPKM et option pour indiquer 1 bin --> 1 base
		echo -ne "#!/bin/sh\\n" > script_temp.sh
		echo -ne "TMPDIR='${TEMP_DIRECTORY}'\\n" >> script_temp.sh
		echo -ne "bamCoverage --bam $i --normalizeUsing RPKM --binSize 1 --numberOfProcessors 4 --outFileName ${OUTPUT_DIRECTORY}/${file_name}.normalized.bw" >> script_temp.sh
		sbatch -c 4 -w node001 script_temp.sh
	else
		echo "File doesn't exist. Create it or enter the right path and file name" >&2
	fi
done

rm script_temp.sh