#!/bin/bash

directory=$1

if [ "${directory}" != "Riesling" ] && [ "${directory}" != "Gewurztraminer" ]; then
    echo "Argument must be 'Riesling' or 'Gewurztraminer'"
    exit 1
fi

cd /data2/malahaye/jbrowse-components/products/jbrowse-web/public/data/${directory}/pseudomolecule/2._RNA-Seq/2.1_Alignments/

for i in *.bam
do
	echo "Indexing $i..."
	samtools index $i
done
