#!/usr/bin/env python3
# coding: utf-8
import argparse

def command_line():
    parser = argparse.ArgumentParser(description='''
        Generate BED file from coords file containing positions
        and identity percentage informations
        ''')
    parser.add_argument('-i', '--input', required=True, help='COORDS file')
    parser.add_argument('-o', '--output', required=True, help='BED file')
    args = parser.parse_args()
    return args.input, args.output


finput, foutput = command_line()
input_file = open(finput, 'r')
output_file = open(foutput, 'w')
for line in input_file:
    line_list = [line[:-1].split('\t')[i] for i in [9, 0, 1, 10, 6]]
    new_line = '\t'.join(line_list)
    new_line += '\n'
    output_file.write(new_line)

input_file.close()
output_file.close()
