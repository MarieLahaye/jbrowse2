#!/usr/bin/env python3
# coding: utf-8
import gffutils
import sqlite3
import warnings


# on ignore le warning "RessourceWarning : unclosed file" ...
# ... qui est dû au module "gffutils" qui ne ferme pas ...
# ... le ficher à partir duquel la bdd est créée
warnings.filterwarnings('ignore', category=ResourceWarning)


# Création de la feature table
def create_db(finput):
    # Cette ligne permet de changer le comportement par défaut de gffutils ...
    # ... ici on demande à ce qu'une liste ne soit pas retournée s'il n'y a qu'un item dedans
    try:
        db = gffutils.create_db(finput, 'gff_db', merge_strategy='create_unique', keep_order=True)
    except  (sqlite3.OperationalError):
        print("Warning: Table features already exists. Supress it if you want to create a new table.\n")
    db = gffutils.FeatureDB('gff_db')
    return db


# Renvoie la liste de tous les enfants (on considère qu'il n'y a pas de gènes dupliqués)
def get_children(gff_db, gene_id):
    # gff_db.children(gene_id) est un générateur, donc il faut le parcourir et mettre tous les ...
    # ... éléments dans une liste
    return [child for child in gff_db.children(gene_id)]


def write_gff(foutput, list_gff_lines):
    output_file = open(foutput, 'w')
    for line in list_gff_lines:
        output_file.write(line)
    output_file.close()