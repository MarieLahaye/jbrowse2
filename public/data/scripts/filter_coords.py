#!/usr/bin/env python3
# coding: utf-8
import argparse
import pandas as pd

def command_line():
    parser = argparse.ArgumentParser(description='''
        Filter COORDS file by removing wrong alignments and haplotigs phase 0
        with alignment with 100% identity and length almost equal to 
        haplotig length
        ''')
    parser.add_argument('-c', '--coords', required=True, help='COORDS file')
    parser.add_argument('-i', '--intersect', required=True, 
                        help='intersect file between haplotigs and contigs')
    parser.add_argument('-o', '--output', required=True, help='BED file')
    args = parser.parse_args()
    return args.coords, args.intersect, args.output


def wrong_alignments(coords, intersect, output):
    coords_df = pd.read_csv(coords, sep='\t', header=None)
    intersect_df = pd.read_csv(intersect, sep='\t', header=None)
    drop_index = []

    n = len(coords_df)
    for i in range(0, n):
        hstart = coords_df.iloc[i, 0]
        hend = coords_df.iloc[i, 1]
        hscore = coords_df.iloc[i, 6]
        hchrom = coords_df.iloc[i, 9]
        hid = coords_df.iloc[i, 10]
        hlength = coords_df.iloc[i, 8]

        # recherche de l'alignement d'un haplotig dans le intersect.tab
        list_intersect = intersect_df[(hid == intersect_df[3])
                & (hstart == intersect_df[1])
                & (hend == intersect_df[2])
                & (hchrom == intersect_df[0])
            ].values

        # regarde dans les intersections trouvées si l'alignement de l'haplotig est ...
        # ... fait sur le bon contig primaire
        if not hid[:7] in [l[8][:7] for l in list_intersect]:
            drop_index.append(i)

    coords_df = coords_df.drop(drop_index)
    return coords_df.reset_index(drop=True)


# filtre les haplotigs dont 1 alignement est à 100% et la longueur = len(haplotig) ...
# ... +/- 100 bases
def filter_phase_0(coords_df):
    n = len(coords_df)
    drop_id = set()
    for i in range(0, n):
        hid = coords_df.iloc[i, 10]
        hstart = coords_df.iloc[i, 0]
        hend = coords_df.iloc[i, 1]
        hscore = coords_df.iloc[i, 6]
        hlength = coords_df.iloc[i, 8]
        if "phase_0" in hid:
            if hscore == 100.0 and abs(hend-hstart) in range(hlength-100, hlength+100):
                drop_id.add(hid)
    coords_df = coords_df[~coords_df[10].isin(drop_id)]
    return coords_df


if __name__ == "__main__":
    fcoords, fintersect, foutput = command_line()
    coords_df = wrong_alignments(fcoords, fintersect, foutput)
    coords_df = filter_phase_0(coords_df)
    coords_df.to_csv(foutput, sep='\t', header=False, index=False)
