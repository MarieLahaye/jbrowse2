#!/usr/bin/env python3
# coding: utf-8
import argparse
import fastaparser
import pandas as pd
from datetime import datetime

def command_line():
    parser = argparse.ArgumentParser(description='''
        Generate BED file from fasta file containing positional information
        ''')
    parser.add_argument('-i', '--input', required=True, help='FASTA file')
    parser.add_argument('-o', '--output', required=True, help='BED file')
    args = parser.parse_args()
    return args.input, args.output


def write_output(finput, foutput):
    input_file = open(finput, 'r')
    sequences = fastaparser.Reader(input_file)
    output_list = [] 

    # seq est un objet de type FastaSequence
    for seq in sequences:
        id_list = seq.id.split('|')
        name = id_list[0]

        # création d'un dictionnaire pour ne pas tenir compte de l'ordre dans ...
        # ... lesquelles sont placées les informations
        id_dict = dict((i, j) for i, j in (k.split('=') for k in id_list[1:-1]))
        chrom = id_dict['chr']
        start = id_dict['begin']
        end = id_dict['end']
        seq_list = [chrom, start, end ,name]
        output_list.append(seq_list)
    
    input_file.close()

    # création d'une dataframe contenant toutes les lignes à écrire ensuite dans ...
    # ... le fichier de sortie
    output_df = pd.DataFrame(output_list)
    output_df.to_csv(foutput, sep='\t', header=False, index=False)


if __name__ == "__main__":
    finput, foutput = command_line()
    write_output(finput, foutput)