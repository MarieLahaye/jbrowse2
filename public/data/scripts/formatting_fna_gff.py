#!/usr/bin/env python3
# coding: utf-8
import argparse
import fastaparser
import pandas as pd
import os
from functions_gffutils import create_db, get_children, write_gff


def command_line():
    parser = argparse.ArgumentParser(description='''
        Filter phase 0 or add attributes 'id_ref' to gff and fasta files by 
        giving a list of gene ids (filter) or list of ref ID and their corresponding
        alternative ID
        ''')
    parser.add_argument('action', choices=['filter', 'add-attribute'])
    parser.add_argument('-i', '--input', help='FASTA or GFF input file', required=True)
    parser.add_argument('-g', '--geneList', help='''
        filter (fna) : list of phase_0 genes you want to keep.
        add-attribute : TSV file containing match between genes ref and alt.
        This argument is required to filter FNA or add attributes to GFF3
        ''')
    parser.add_argument('-b', '--haplotigs', help='''
        Bed file with haplotigs phase 0 coordinates.
        This argument is required to filter GFF3 files. This script will filter genes (phase 0)
        based on the coordinates of the haplotigs given in input.
        ''')
    parser.add_argument('-o', '--output', help='FASTA or GFF output file', required=True)
    args = parser.parse_args()
    return args


def filter_phase_0_gff_old(finput, fgeneList, foutput):
    gff_df = pd.read_csv(finput, sep='\t', header=None, comment='#')
    genes_df = pd.read_csv(fgeneList, sep='\t', header=None)

    drop_index = []
    for i in range(0, len(gff_df)):
        if "phase_0" in gff_df.iloc[i, 0]:
            gene_id = [g[8:] for g in gff_df.iloc[i, 8].split(';') if "ID" in g]
            genes_res = genes_df[genes_df[0] == gene_id[0]]
            if genes_res.size == 0:
                drop_index.append(i)
    gff_df = gff_df.drop(drop_index)
    gff_df.to_csv(foutput, sep='\t', header=False, index=False)


# Fonction pour filtrer les gènes localisés sur des haplotigs phase 0
# Certains haplotigs ont été complètement supprimés car ils étaient identique à la séquence ...
# ... de référence, tandis que certaines portions d'autres haplotigs on été supprimées
# On va donc supprimer les gènes localisés sur des haplotigs phase 0 qui n'ont pas été conservés ...
# ... ou bien sur des portions d'haplotigs qui ont été filtrées
# Les coordonnées des haplotigs indiquent les portions qui ont été conservées, dans le cas où ...
# ... le début d'un haplotig a été filtré, alors il faudra recalculer les coordonnées des gènes ...
# ... pour qu'ils correspondent aux nouveaux coordonnées des haplotigs (le nouveau start de ...
# ... de l'haplotig, qui peut très bien débuter en plein milieu de celui-ci, devient la nouvelle ...
# ... position 0)
def filter_phase_0_gff(finput, fhaplotigs, foutput):
    # Création d'une dataframe contenant les coordonnées des haplotigs, et set index nom des ...
    # ... haplotigs
    haplotigs_df = pd.read_csv(fhaplotigs, sep='\t', header=None)
    haplotigs_df_with_index = haplotigs_df.set_index(0)

    # Chargement du gff
    gff_db = create_db(finput)
    list_gff_lines = []
    
    for gene in gff_db.features_of_type("gene"):
        gene_id = gene['ID'][0]
        # Si le gène est sur un haplotig phase 0 --> filtre ou calcul des nouvelles coordonnées
        if "phase_0" in gene_id:
            haplotig_name = gene_id.replace('gene:', '').split('g')[0]
            # Si le l'haplotig du gène est dans la liste, on recherche les enfants, puis calcul des ...
            # ... nouvelles coordonnées et ajout à la liste des lignes qui seront écrites dans le ...
            # ... fichier de sortie
            # Sinon, on ne garde pas ce gène et ses enfants 
            if haplotig_name in haplotigs_df_with_index.index.to_list():
                haplotig_start = haplotigs_df_with_index.loc[haplotig_name, :].to_list()[0]
                haplotig_end = haplotigs_df_with_index.loc[haplotig_name, :].to_list()[1]
                # Check si les coordonnées du gènes sont bien comprises dans la portion d'haplotig ...
                # ... qui n'a pas été filtrée, sinon il ne faut pas garder le gène
                if gene.start in range(haplotig_start, haplotig_end+1) \
                or gene.end in range(haplotig_start, haplotig_end+1):
                    for feature in [gene, *get_children(gff_db, gene_id)]:
                        feature.start -= haplotig_start
                        feature.end  -= haplotig_start
                        if feature.start <= 0:
                            feature.start = 1
                        if feature.end <= 0:
                            feature.end = 1
                        list_gff_lines.append('{}\n'.format(str(feature)))
        else:
            for feature in [gene, *get_children(gff_db, gene_id)]:
                list_gff_lines.append('{}\n'.format(str(feature)))

    # écriture du fichier gff avec les attributs id_ref ajoutés
    write_gff(foutput, list_gff_lines)


# Création d'une Séries pandas contenant la liste des correspondances entre les ...
# ... gènes ref et alt
def get_genes_correspondance(fgenelist):
    dict_genes_correspondance = {}
    with open(fgenelist, 'r') as genelist_file:
        count = 0
        for gene in genelist_file:
            split_gene = gene[:-1].split('\t')
            alt, ref = split_gene[:2]
            haplotig = ""
            if split_gene[2]:
                haplotig = split_gene[2]
            if alt in dict_genes_correspondance.keys():
                count += 1
                dict_genes_correspondance[alt].append([ref, haplotig])
            else:
                dict_genes_correspondance[alt] = [[ref, haplotig]]
    return dict_genes_correspondance


# Fonction permettant d'ajouter un attribut 'id_ref' correspondant aux correspondances ...
# ... des gènes sur la pseudomolécule
def add_attribute_gff(finput, fgenelist, foutput):
    genes_list_correspondance = get_genes_correspondance(fgenelist)
    gff_db = create_db(finput)
    list_gff_lines = []
    
    for gene in gff_db.features_of_type("gene"):
        alt_name = gene['Name'][0]
        alt_id = gene['ID'][0]
        # Si id_name dans la liste des correspondances --> ajoute attributs id_ref
        if alt_name in genes_list_correspondance.keys():
            list_id_ref = []
            correspondances = genes_list_correspondance[alt_name]
            for c in correspondances:
                list_id_ref.append(c[0])
                if c[1]:
                    gene['haplotig'] = 'partiel'
            gene['id_ref'] = list_id_ref
        list_gff_lines.append('{}\n'.format(str(gene)))
        children = get_children(gff_db, alt_id)
        for child in children:
            list_gff_lines.append('{}\n'.format(str(child)))

    # écriture du fichier gff avec les attributs id_ref ajoutés
    write_gff(foutput, list_gff_lines)
    

def filter_phase_0_fasta(finput, fgenelist, foutput):
    input_file = open(finput, 'r')
    sequences = fastaparser.Reader(input_file)
    genes_df = pd.read_csv(fgenelist, sep='\t', header=None)

    seq_list = []
    # parcours des séquences et check si le gène vient ...
    # d'un haplotigs phase 0
    for seq in sequences:
        if "phase_0" in seq.id:
            genes_res = genes_df[genes_df[0] == seq.id].values
            # si gène retrouvé dans la liste, on le garde
            if len(genes_res) > 0:
                seq_list.append(seq)
        # et on garde aussi tout ceux qui ne viennent pas ...
        # ... d'un haplotig phase 0
        else:
            seq_list.append(seq)
    input_file.close()

    # écriture du fasta filtré
    write_fasta(seq_list, foutput)


def rename_fasta(finput, fgenelist, foutput):
    # chargement du fichier fasta
    input_file = open(finput, 'r')
    sequences = fastaparser.Reader(input_file)

    # chargement des correspondances ref/alt
    genes = pd.read_csv(fgenelist, sep='\t', header=None)

    # modification de l'id alt en ref si correspondance
    seq_list = []
    for seq in sequences:
        genes_res = genes[genes[0] == seq.id].values
        # si correspondance, renomme l'id de la séquence
        if len(genes_res) > 0: 
            for g in genes_res:
                seq.id = "{0}".format(g[1])
                seq_list.append(seq)
        else:
            seq_list.append(seq)
    input_file.close()

    # ecrite du fichier de sortie contenant les séquences renommées
    write_fasta(seq_list, foutput)


def write_fasta(output, foutput):
    output_file = open(foutput, 'w')
    writer = fastaparser.Writer(output_file)
    for seq in output:
        writer.writefasta(seq)
    output_file.close()
    

if __name__ == '__main__':
    args = command_line()

    extension = os.path.basename(args.input).split('.')[-1]
    ext_fasta = {'fa', 'fna'}
    if extension == "gff3":
        if args.action == 'filter':
            filter_phase_0_gff(args.input, args.haplotigs, args.output)
        elif args.action == 'add-attribute':
            add_attribute_gff(args.input, args.geneList, args.output)
    elif extension in ext_fasta:
        if args.action == 'filter':
            filter_phase_0_fasta(args.input, args.geneList, args.output)
        elif args.action == 'add-attribute':
            rename_fasta(args.input, args.geneList, args.output)