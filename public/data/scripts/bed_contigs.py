#!/usr/bin/env python3
# coding: utf-8
import argparse

def command_line():
    parser = argparse.ArgumentParser(description='''
        Generate BED file of primary contigs from .txt file
        ''')
    parser.add_argument('-i', '--input', required=True, help='TXT file')
    parser.add_argument('-o', '--output', required=True, help='BED file')
    args = parser.parse_args()
    return args.input, args.output

finput, foutput = command_line()
input_file = open(finput, 'r')
output_file = open(foutput, 'w')
for line in input_file:
	if line[0] == ">":
		start = 0
		# récupère le nom du chromosome sans garder le ">"
		chrom = line.split('\t')[0][1:]
	else:
		contig = line[:-1].split('\t')
		end = start + int(contig[2])
		name = contig[0]
		new_line = "{}\t{}\t{}\t{}\n".format(chrom, start, end, name)
		start = end + int(contig[3])
		output_file.write(new_line)

input_file.close()
output_file.close()
