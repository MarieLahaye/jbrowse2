#!/bin/bash

# Fonction to display the script help
function usage {
	cat <<-__EOF__
		Usage:
			./bedsashimi.sh -i input_files -o outdir [-h]

		Description:
			Generation BED files containing exons junctions based on BAM files

		Arguments:
			-i, --input List of BAM files (required)
				Files must be separated by commas
				You can use the following command: ls path/to/*.bam | tr "\n" ","
			-o, --outdir Output directory  (required)
			-h, --help

		Exemple: ./bedsashimi.sh -i \`ls *.bam | tr "\n" ","\` -o path/to/outdir
		__EOF__
}

# Eval command line arguments given in input
ARGS=$(getopt -o "i:o:h" --long "input:,outdir:,help" -- $@ 2> /dev/null)

# Check if the return code of the previous command is not equal to 0 (command ...
# ... didn't work)
# >&2 send the message to standard error (stderr) instead of standard out (stdout)
if [ $? -ne 0 ]; then
	echo "Error in the argument list. Use -h or --help to display the help." >&2
	exit 1
fi

eval set -- ${ARGS}
while true
do
	case $1 in
		-i|--input)
			INPUT_FILES=$2
			shift 2
			;;
		-o|--outdir)
			OUTPUT_DIRECTORY=$2
			shift 2
			;;
		-h|--help)
			usage
			exit 0
			;;
		--) shift
			break
			;;
		*)	echo "Option $1 is not recognized. Use -h or --help to display the help." && \
			exit 1
			;;
	esac
done

# Check if one required parameter is missing in the given command line
if [[ ${INPUT_FILES} == "" ]] || [[ ${OUTPUT_DIRECTORY} == "" ]]; then
	echo "Options --input, --fasta and --outdir are required. Use -h or --help to display the help" >&2
	exit 1
fi

# Check if directory exists
if ! [ -d "${OUTPUT_DIRECTORY}" ]; then
    echo "${OUTPUT_DIRECTORY} doesn't exist. Create it or enter the right path and file name" >&2
    exit 1
fi

module load regtools/0.5.2

for i in $(echo ${INPUT_FILES} | tr "," "\n")
do
	if [ -f $i ]; then
		file=${i##*/}
		file_name=${file%%.*}
		echo "Extracting junction from $i..."
		# récupération des jonctions entre les exons (correspondent aux jonctions entre les reads d'une même paire)

		regtools junctions extract -s 2 -t XS $i -o ${OUTPUT_DIRECTORY}/${file_name}.ori.junctions.bed
		# filtre les jonctions supportées par moins de 3 reads, puis recalcule les bonnes positions start et end
		/data2/malahaye/jbrowse-components/products/jbrowse-web/public/data/scripts/junctions_positions.py -i ${OUTPUT_DIRECTORY}/${file_name}.ori.junctions.bed -o ${OUTPUT_DIRECTORY}/${file_name}.junctions.bed
		# tri, compression, indexation
		sortBed -i ${OUTPUT_DIRECTORY}/${file_name}.junctions.bed > ${OUTPUT_DIRECTORY}/${file_name}.junctions.sorted.bed
		bgzip ${OUTPUT_DIRECTORY}/${file_name}.junctions.sorted.bed
		tabix -p bed ${OUTPUT_DIRECTORY}/${file_name}.junctions.sorted.bed.gz
	else
		echo "File doesn't exist. Create it or enter the right path and file name" >&2
	fi
done
