#!/usr/bin/env python3
# coding: utf-8
import argparse
import json
import os.path
import operator

# Fonction pour définir tous les arguments et options de la ligne de commande
def command_line():
    parser = argparse.ArgumentParser(description='''
        Management of the tracks of the configuration file 'config.json' of jbrowse2.
        Different options allow you to create tracks and add metadata, delete or
        update them.
        The name of the track will match the name of the file and the category is
        determined by the directory in which the file is located. The '_' in the folder
        name will be converted to spaces.
        ''')
    parser.add_argument(
        '-c', '--conf', required=True,
        help='''
        Tracks configuration file: trackList.json (jbrowse) or config.json
        (jbrowse2) - REQUIRED
        '''
        )
    parser.add_argument(
        '-a', '--add', nargs='+',
        help='''
        List of files for which you want to create tracks
        '''
        )
    parser.add_argument(
        '-n', '--assemblyName', required=True,
        help='''
        Assembly name corresponding to the data track you want to add - REQUIRED
        '''
        )
    parser.add_argument(
        '-r', '--remove', nargs='+',
        help='list of tracks label to remove from the jbrowse'
    )
    parser.add_argument(
        '-m', '--metadata',
        help='''
        JSON file containing the list of metadata corresponding to the different tracks.
        The key of each metadata must correspond to the name of the track
        (name of the associated file)
        '''
    )
    parser.add_argument(
        '--update', action='store_true',
        help='to update existing tracks by giving the files path'
    )
    args = parser.parse_args()
    return args


# Récupération du nom du fichier bam sans extension, de l'extension ...
# ... du dossier parent et du chemin du fichier à mettre dans l'url du tracks
def path_name(file, json_path):
    name = os.path.basename(file).split('.')[0]
    extension = os.path.basename(file).split('.')[-1]
    if extension == "gz":
        extension = os.path.basename(file).split('.')[-2]
    file_path = os.path.abspath(file).replace(
        os.path.commonprefix(
            [os.path.abspath(file), os.path.abspath(json_path)]
        ), "")
    directory = os.path.dirname(file).split('/')[-1]
    if not directory:
        absolute_path = os.path.abspath(file)
        directory = os.path.dirname(absolute_path).split('/')[-1]
        file_path = "{0}/{1}".format(directory, file_path)
    return name, extension, file_path, directory


# Suppression track en fonction de son label, ou de son chemin --> dans le...
# ... cas où l'on veut mettre à jour les tracks
def remove_track(name, data, update, extension=None, file_path=None, assembly_name=None):
    uri_name = {
        "gff3": "gffGzLocation",
        "bam": "bamLocation",
        "bw": "bigWigLocation",
        "bb": "bigBedLocation",
        "bed": "bedGzLocation"
    }
    for track in data['tracks'][:]:
        if update:
            # Comme le "chemin" vers l'uri n'est pas la même en fonction ...
            # ... du format de fichier, il faut aussi le tester, avant de checker ...
            # ... si le chemin est le même
            if uri_name[extension] in track['adapter'].keys() and \
               track['adapter'][uri_name[extension]]['uri'] == file_path:
                data['tracks'].remove(track)
                break
        else:
            if track['name'] == name and assembly_name in track['assemblyNames']:
                data['tracks'].remove(track)


def create_categories_rnaseq(track_type, organ, tracks_categories, projects):
    for p in projects:
        tracks_categories['2. RNA-Seq/{0}/{1}/{2}'.format(track_type, organ, p)] = []


# Fonction pour regrouper les tracks en fonction de la catégorie à laquelle...
# ... ils sont associés
def tracks_list(data):
    trackList = {
        "0. Reference Sequence": [],
        "1. Annotations/1.1 Riesling annotations": [],
        "1. Annotations/1.1 Gewurztraminer annotations": [],
        "1. Annotations/1.2 PN12Xv2 annotations": [],
        "1. Annotations/1.3 PN40024.v4 annotations": [],
        "3. WGS": [],
        "4. Haplotigs": [],
        "5. Others": []
    }

    # Catégories RNA-Seq
    for c in ['2.1 Alignments', '2.2 BigWig XY', '2.3 Sashimi plot']:
        for o in ['2.1.1 Berries', '2.1.2 Wood']:
            create_categories_rnaseq(c, o, trackList, ['PRJEB45016'])
            create_categories_rnaseq(c, o, trackList, ['PRJEB11405', 'PRJEB43358', 'PRJNA383160', 'PRJNA770860'])


    #for track in data['tracks']:
    #    if '/'.join(track['category']) not in trackList.keys():
    #        trackList['/'.join(track['category'])] = []
    # Parcours des assemblages
    for assembly in data['assemblies']:
        trackList['0. Reference Sequence'].append(assembly)

    # Parcours des tracks
    for track in data['tracks']:
        joined_categories = '/'.join(track['category'])
        if joined_categories in trackList.keys():
            trackList[joined_categories].append(track)
    return trackList


# Récupération du chemin vers le fichier FASTA de la séquence de ref des pseudomolécules ...
# ... ou des haplotigs
def get_assembly_path(data, assembly):
    assembly_dict = [a for a in data['assemblies'] if a['name'] == assembly][0]
    return assembly_dict['sequence']['adapter']['fastaLocation']['uri']


# Ajout/Remplacement metadata et création des tracks
def track(data, assembly, metadata_file, directory, file_path, name,
          extension, trackList, update_message):
    category = directory.replace("_", " ")

    # Création des tracks adaptées, en fonction de l'extension du fichier
    if extension == "fa":
        if assembly not in [t['name'] for t in trackList['0. Reference Sequence']]:
            create_refseq_track(file_path, data, assembly)
        else:
            update_message = upd_message(name, category, update_message)
    elif extension == "bam":
        if "3._WGS" in file_path:
            if "{0}_{1}".format(name, assembly.replace(" ", "_")) not in [t['trackId'] for t in trackList[category]]:
                create_bam_track(name, category, file_path, data, assembly, metadata_file)
            else:
                update_message = upd_message(name, category, update_message)
        else:
            if "merged" in name:
                name = "{}_RNA-seq_data".format(name)
            category = ["2. RNA-Seq/2.1 Alignments/{0}/{1}".format(organ.replace("_", " "), category.replace("_", " ")) for organ in ['2.1.1_Berries', '2.1.2_Wood'] if organ in file_path][0]
            if "{0}_alignments_{1}".format(name, assembly.replace(" ", "_")) not in [t['trackId'] for t in trackList[category]]:
                create_bam_track(name, category, file_path, data, assembly, metadata_file)
            else:
                update_message = upd_message(name, category, update_message)
    elif extension == "bw":
        if "merged" in name:
            name = "{}_RNA-seq_data".format(name)
        category = ["2. RNA-Seq/2.2 BigWig XY/{0}/{1}".format(organ.replace("_", " "), category.replace("_", " ")) for organ in ['2.1.1_Berries', '2.1.2_Wood'] if organ in file_path][0]
        if "{0}_bigwig_{1}".format(name, assembly.replace(" ", "_")) not in [t['trackId'] for t in trackList[category]]:
            create_wig_track(name, category, file_path, data, assembly, metadata_file)
        else:
            update_message = upd_message(name, category, update_message)
    elif extension == "gff3":
        annotations_id = "{0}_{1}".format(name, assembly.replace(" ", "_"))
        if annotations_id not in [t['trackId'] for t in trackList['1. Annotations/{}'.format(category)]]:
            create_gff_track(name, category, file_path, data, assembly, metadata_file)
        else:
            update_message = upd_message(name, category, update_message)
    elif extension == "bed":
        if "Sashimi" in file_path:
            if "merged" in name:
                name = "{}_RNA-seq_data".format(name)
            category = ["2. RNA-Seq/2.3 Sashimi plot/{0}/{1}".format(organ.replace("_", " "), category.replace("_", " ")) for organ in ['2.1.1_Berries', '2.1.2_Wood'] if organ in file_path][0]
            if "{0}_sashimi_{1}".format(name, assembly.replace(" ", "_")) not in [t['trackId'] for t in trackList[category]]:
                create_sashimi_track(name, category, file_path, data, assembly, metadata_file)
            else:
                update_message = upd_message(name, category, update_message)
        elif "{0}_{1}".format(name, assembly.replace(" ", "_")) not in [t['trackId'] for t in trackList[category]]:
            if "Haplotigs" in name:
                create_haplotigs_track(name, category, file_path, data, assembly, metadata_file)
            elif "Alleles" in name:
                create_allele_track(name, category, file_path, data, assembly, metadata_file)
            else:
                update_message = upd_message(name, category, update_message)
    elif extension == "bb":
        if "{0}_{1}".format(name, assembly.replace(" ", "_")) not in [t['trackId'] for t in trackList[category]]:
            create_bb_track(name, category, file_path, data, assembly, metadata_file)
        else:
            update_message = upd_message(name, category, update_message)
    return update_message


# Fonction pour récupérer les métadonnées correspondant à une track en fonction de son nom
def get_metadata(track_name, metadata_file):
    with open(metadata_file, 'r') as meta_file:
        metadata_load = json.load(meta_file)
        if track_name in metadata_load.keys() : return metadata_load[track_name]


def create_base_config():
    track = {
        "configuration": {
            "theme": {
                "palette": {
                    "primary": {
                        "main": "#004b4d"
                    },
                    "secondary": {
                        "main": "#009699"
                    },
                    "tertiary": {
                        "main": "#006366"
                    },
                    "quaternary": {
                        "main": "#c4815a"
                    }
                }
            }
        },
        "assemblies": [],
        "connections": [],
        "tracks": [],
        "aggregateTextSearchAdapters": [],
        "defaultSession": {
            "name": "New Session"
        }
    }
    json.dump(track, json_file, indent=4)


def create_refseq_track(path, data, assembly):
    track = {
        "name": assembly,
        "sequence": {
            "type": "ReferenceSequenceTrack",
            "trackId": "{}_ReferenceSequenceTrack".format(assembly.replace(" ", "_")),
            "adapter": {
                "type": "IndexedFastaAdapter",
                "fastaLocation": {
                    "uri": path
                },
                "faiLocation": {
                    "uri": "{}.fai".format(path)
                }
            }
        }
    }
    data['assemblies'].append(track)


# Création des tracks pour les fichiers gff
def create_gff_track(name, category, path, data, assembly, metadata_file):
    track = {
        "type": "FeatureTrack",
        "trackId": "{0}_{1}".format(name, assembly.replace(" ", "_")),
        "name": name.replace("_", " "),
        "category": [
            "1. Annotations",
            "{}".format(category)
        ],
        "assemblyNames": [
            assembly
        ],
        "adapter": {
            "type": "Gff3TabixAdapter",
            "gffGzLocation": {
                "uri": path
          },
            "index": {
                "location": {
                    "uri": "{}.tbi".format(path)
            },
                "indexType": "TBI"
            }
        }
    }

    if metadata_file:
        name = track['name']
        metadata_track = get_metadata(name, metadata_file)
        if metadata_track:
            track['description'] = metadata_track
    data['tracks'].append(track)


# Création des tracks pour les fichiers bam
def create_bam_track(name, category, path, data, assembly, metadata_file):
    if "3._WGS" in path:
        trackID = "{0}_{1}".format(name, assembly.replace(" ", "_"))
        name = name.replace("_", " ")
        category = [category]
    elif "2._RNA-Seq" in path:
        trackID = "{0}_alignments_{1}".format(name, assembly.replace(" ", "_"))

    fasta_path = get_assembly_path(data, assembly)
    track = {
        "type": "AlignmentsTrack",
        "trackId": trackID,
        "name": name,
        "category": category.split('/'),
        "assemblyNames": [
            assembly
        ],
        "adapter": {
            "type": "BamAdapter",
            "bamLocation": {
                "uri": path
            },
            "index": {
                "location": {
                    "uri": "{}.bai".format(path)
                },
                "indexType": "BAI"
            },
            "sequenceAdapter": {
                "type": "IndexedFastaAdapter",
                "fastaLocation": {
                    "uri": fasta_path
                },
                "faiLocation": {
                    "uri": "{}.fai".format(fasta_path)
                }
            }
        }
    }

    if metadata_file:
        metadata_track = get_metadata(name, metadata_file)
        if metadata_track:
            track['description'] = metadata_track
    data['tracks'].append(track)


# Création des tracks pour les fichiers wig
def create_wig_track(name, category, path, data, assembly, metadata_file):
    track = {
      "type": "QuantitativeTrack",
      "trackId": "{0}_bigwig_{1}".format(name, assembly.replace(" ", "_")),
      "name": name,
      "category": category.split('/'),
      "assemblyNames": [
        assembly
      ],
      "adapter": {
        "type": "BigWigAdapter",
        "bigWigLocation": {
          "uri": path
        }
      }
    }

    if metadata_file:
        metadata_track = get_metadata(name, metadata_file)
        if metadata_track:
            track['description'] = metadata_track
    data['tracks'].append(track)


# Création de la track des sashimi plot
def create_sashimi_track(name, category, path, data, assembly, metadata_file):
    track = {
        "type": "BasicTrack",
        "trackId": "{0}_sashimi_{1}".format(name, assembly.replace(" ", "_")),
        "name": name,
        "category": category.split('/'),
        "assemblyNames": [
            assembly
        ],
        "adapter": {
            "type": "BedTabixAdapter",
            "bedGzLocation": {
                "uri": path
            },
            "index": {
                "location": {
                    "uri": "{}.tbi".format(path)
                },
                "indexType": "TBI"
            }
        },
        "displays": [
            {
                "type": "LinearArcDisplay",
                "displayId": "{0}_{1}_display".format(name, assembly.replace(" ", "_")),
                "renderer": {
                    "type": "ArcRenderer",
                    "color": "jexl:colorGradientArcsJunctions(feature)"
                }
            }
        ]
    }

    if metadata_file:
        metadata_track = get_metadata(name, metadata_file)
        if metadata_track:
            track['description'] = metadata_track
    data['tracks'].append(track)


def create_haplotigs_track(name, category, path, data, assembly,metadata_file):
    track = {
        "type": "FeatureTrack",
        "trackId": "{0}_{1}".format(name, assembly.replace(" ", "_")),
        "name": name.replace("_", " "),
        "category": [
            category
        ],
        "assemblyNames": [
            assembly
        ],
        "adapter": {
            "type": "BedTabixAdapter",
            "bedGzLocation": {
            "uri": path
            },
            "index": {
                "location": {
                    "uri": "{}.tbi".format(path)
                },
                "indexType": "TBI"
            }
        },
        "displays": [
            {
                "type": "LinearBasicDisplay",
                "displayId": "{0}_{1}_display".format(name, assembly.replace(" ", "_")),
                "renderer": {
                    "type": "SvgFeatureRenderer",
                    "color1": "jexl:colorGradientHaplotigsOrAlleles(feature, 'Haplotigs')"
                },
                "mouseover": "jexl:retrieveScore(feature)"
            }
        ]
    }

    if metadata_file:
        metadata_track = get_metadata(name.replace("_", " "), metadata_file)
        if metadata_track:
            track['description'] = metadata_track
    data['tracks'].append(track)


def create_allele_track(name, category, path, data, assembly, metadata_file):
    track = {
        "type": "FeatureTrack",
        "trackId": "{0}_{1}".format(name, assembly.replace(" ", "_")),
        "name": name.replace("_", " "),
        "category": [
            category
        ],
        "assemblyNames": [
            assembly
        ],
        "adapter": {
            "type": "BedTabixAdapter",
            "bedGzLocation": {
                "uri": path
            },
            "index": {
                "location": {
                    "uri": "{}.tbi".format(path)
                },
                "indexType": "TBI"
            }
        },
        "displays": [
            {
                "type": "LinearBasicDisplay",
                "displayId": "{0}_{1}_display".format(name, assembly.replace(" ", "_")),
                "renderer": {
                    "type": "SvgFeatureRenderer",
                    "color1": "jexl:colorGradientHaplotigsOrAlleles(feature, 'Alleles')"
                },
                "mouseover": "jexl:retrieveScore(feature)"
            }
        ]
    }

    if metadata_file:
        metadata_track = get_metadata(name.replace("_", " "), metadata_file)
        if metadata_track:
            track['description'] = metadata_track
    data['tracks'].append(track)


def create_bb_track(name, category, path, data, assembly, metadata_file):
    track = {
        "type": "FeatureTrack",
        "trackId": "{0}_{1}".format(name, assembly.replace(" ", "_")),
        "name": name.replace("_", " "),
        "category": [
            category
        ],
        "assemblyNames": [
            assembly
        ],
        "adapter": {
            "type": "BigBedAdapter",
            "bigBedLocation": {
                "uri": path
            }
        }
    }

    if "N_gaps" in name:
        track['displays'] = [
            {
                "type": "LinearBasicDisplay",
                "displayId": "{}_display".format(track['trackId']),
                "renderer": {
                    "type": "SvgFeatureRenderer",
                    "color1": "#C4C4C4",
                    "showLabels": False
                },
                "mouseover": "jexl:lengthFeature(feature)+'b'"
            }
        ]

    if metadata_file:
        metadata_track = get_metadata(name.replace("_", " "), metadata_file)
        if metadata_track:
            track['description'] = metadata_track
    data['tracks'].append(track)


# Stockage dans une liste, des messages à renvoyer si un track existe déjà...
# ... dans le cas où l'option '--update' n'a pas été indiquée
def upd_message(name, category, message):
    if message:
        message.append(
            "{0} track in {1} category already exists".format(name, category)
        )
    else:
        message = [
            "{0} track in {1} category already exists".format(name, category)
        ]
    return message


args = command_line()
update_message = None

if __name__ == "__main__":
    # Gestion des exceptions:
    # Except permet de traiter le cas où l'erreur suivante est levée...
    # ... ici il s'agit du cas où le fichier json fournit n'est pas valide...
    # ... par exemple s'il est vide ou qu'il n'existe pas
    try:
        with open(args.conf) as json_file:
            data = json.load(json_file)
            pass
    except (json.decoder.JSONDecodeError, FileNotFoundError):
        with open(args.conf, 'w') as json_file:
            create_base_config()

    # Load le fichier json puis regarde quelle action réaliser : ...
    # ... suppression de tracks, création de tracks, récupération du...
    # ... nom ou du chemin du fichier, etc
    with open(args.conf) as json_file:
        data = json.load(json_file)
        if args.remove:
            for name in args.remove:
                remove_track(name, data, args.update, assembly_name=args.assemblyName)

        if args.add:
            for file in args.add:
                name, extension, file_path, directory = path_name(
                    file, args.conf
                )
                if args.update is True:
                    remove_track(name, data, args.update, extension=extension, file_path=file_path)
                trackList = tracks_list(data)
                update_message = track(
                    data, args.assemblyName, args.metadata, directory,
                    file_path, name, extension,
                    trackList, update_message
                )

    if update_message:
        for m in update_message:
            print(m)
        print("Note: If you want to update these tracks,"
              " please use the option '--update'.\n")

    with open(args.conf, 'w') as json_file:
        data['tracks'].sort(key=operator.itemgetter('category'))
        json.dump(data, json_file, indent=4)
