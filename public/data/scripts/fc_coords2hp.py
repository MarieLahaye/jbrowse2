#!/usr/bin/env python3

# Sarah B. Kingan
# 20 September 2017
# 
# Pacific Biosciences
# Applications Lab
#
# Convert nucmer.coords files for all haplotigs aligned
# to primary contigs into ncbi placement file
#
###########################################################################################################

# import libraries
import numpy as np
import subprocess
import argparse

###########################################################################################################

desc ='''Make haplotig placement file from individual coords file of all haplotigs to primary'''
parser = argparse.ArgumentParser(description=desc)
parser.add_argument("inFile", help="coords file")
args = parser.parse_args()

def gap (j, k):
    if j <= k:
        return 0
    else:
        g = int(hd[j,0]) - int(hd[k,1])
        if g < 0:
            return 0
        else:
            return g


# input file
coords_file = args.inFile

# read file 
d=np.genfromtxt(open(coords_file, "rb"), dtype="str")
#coords=[pstart_1 pend hstart_1 hend pAlnL hAlnL PID pL hL pID hID]

# unique set of haplotigs
haplotigs=list(set(list(d[:,10])))
# loop through haplotigs, subset array for each
for h in haplotigs:
    output=['hID','hL', 'hStart_0', 'hEnd', 'ori', 'pID', 'pL', 'pStart_0', 'pEnd', 'match', 'alnL', 'mapQ', 'identity']

    # get data for a haplotig
    subset = []
    for i in range(0,d.shape[0]):
        if d[i,10] == h:
            subset.append(i)    
    n = len(subset)
    hd = d[subset]

    #initialize results array
    results = [{'start_index':-1, 'end_index':-1, 'score': float("-inf")} for l in range(n)]

    # score chained alignments
    identity = 0
    for i in range(0,n):
        score = 0
        # sum % identity for each alignment
        identity += float(hd[i, 6])
        for j in range(i,n):
            score += int(hd[j,4]) * float(hd[j,6]) * 0.01 - gap(j, j-1)
            result_index = j - 1
            if results[result_index]['score'] < score:
                results[result_index]['start_index'] = i
                results[result_index]['end_index'] = j
                results[result_index]['score'] = score

    # get best scoring chained alignment
    max_value = float("-inf")
    best = {'start_index':-1, 'end_index':-1, 'score': float("-inf")}
    for i in range(0, len(results)):
        if results[i]['score'] > max_value:
            best = results[i]
            max_value = best['score']

    identity = 0
    coefficient = 0
    for i in range(best['start_index'], best['end_index']+1):
        identity += float(hd[i, 6])*int(hd[i, 4])
        coefficient += int(hd[i, 4])

    # load data
    output[0]=hd[0,10]  #htig name
    output[5]=hd[0,9]   #pcontig name
    output[6]=hd[0,7]   #pcontig length
    output[1]=int(hd[0,8])   #hcontig length
    output[12]=format(identity/coefficient, '.2f')  # mean % percentage
    hstart=int(hd[best['start_index'],2]) # hstart
    hend=int(hd[best['end_index'],3]) # hend
    pstart=int(hd[best['start_index'],0]) # pstart
    pend=int(hd[best['end_index'],1]) # pend

    if "phase_0" in h:
        # orientation
        output[4] = '+' # ori
        hori = '+'
        pori = '+'
        if hstart > hend:
            hori = '-'
        if pstart > pend:
            pori = '-'
        if hori != pori:
            output[4] = '-'

        if n>1:
            # calcul du % d'identité pour les haplotigs phase_0 ...
            # ... on ne prend pas en compte les alignements à 100% ...
            # ... d'identité, et on se base sur tous les alignements ...
            # ... et pas ceux issus de la meilleure suite d'alignements
            identity = 0
            coefficient = 0
            for i in range(0, n):
                if float(hd[i, 6]) < 100.00:
                    identity += float(hd[i, 6])*int(hd[i, 4])
                    coefficient += int(hd[i, 4])
            output[12] = format(identity/coefficient, '.2f')

            alig1 = list(hd[0])
            align = list(hd[n-1])
            c_coords = alig1[:2] + align[:2]
            h_coords = alig1[2:4] + align[2:4]

            # convertion de toutes valeurs en int
            for i in range(0, 4):
                alig1[i] = int(alig1[i])
                align[i] = int(align[i])
            if alig1[6] == "100.0":
                # alig1[2] = hend et align[3] = hstart
                if alig1[2] > align[3]:
                    # il faut prendre les colonnes associées dans alig1
                    # on cherche le minimum car on veut faire terminer ...
                    # ... l'haplotig juste avant le debut de cet ...
                    # ... alignement
                    index_value = alig1.index(min(alig1[2:4]))-2
                    # hend = start alignement-1
                    output[3] = min(alig1[2:4])-1 
                    pend = alig1[index_value]-1
                    if align[6] == "100.0":
                        index_value = align.index(max(align[2:4]))-2
                        output[2] = max(align[2:4])
                        pstart = align[index_value]
                        # remet le start et le end sur la pseudomol ...
                        # ... dans l'ordre
                        if pstart > pend:
                            output[7] = pend+1
                            output[8] = pstart-1
                        else:
                            output[7] = pstart
                            output[8] = pend
                    else:
                        index_value = align.index(min(align[2:4]))-2
                        output[2] = min(align[2:4])-1
                        pstart = align[index_value]-1
                        # align[0] = pend et align[1] = pstart
                        if pstart > pend:
                            output[7] = pend+1
                            # recalcule le end car est associé au pstart ...
                            # ... du align qui n'est pas à 100% d'identité
                            output[8] = pstart+output[2]+1
                        else:
                            output[7] = pstart-output[2]
                            output[8] = pend
                        output[2] = 0

                # alig1 = start et align = end
                else:
                    index_value = alig1.index(max(alig1[2:4]))-2
                    output[2] = max(alig1[2:4])
                    pstart = alig1[index_value]
                    if align[6] == "100.0":
                        index_value = align.index(min(align[2:4]))-2
                        output[3] = min(align[2:4])-1
                        pend = align[index_value]-1
                        if pstart > pend:
                            output[7] = pend+1
                            output[8] = pstart-1
                        else:
                            output[7] = pstart
                            output[8] = pend
                    else:
                        index_value = align.index(max(align[2:4]))-2
                        output[3] = max(align[2:4])
                        pend = align[index_value]
                        if pstart > pend:
                            # le start vient de l'alignement qui n'est pas ...
                            # ... à 100% d'identité donc on enlève la différence ...
                            # ... entre la longueur de l'haplotig et le end
                            output[7] = pend-(output[1]-output[3])-1
                            output[8] = pstart-1
                        else:
                            output[7] = pstart
                            output[8] = pend+(output[1]-output[3])
                        output[3] = output[1]

            else:
                if alig1[2] > align[3]:
                    index_value = alig1.index(max(alig1[2:4]))-2
                    output[3] = max(alig1[2:4])
                    pend = alig1[index_value]
                    if align[6] == "100.0":
                        index_value = align.index(max(align[2:4]))-2
                        output[2] = max(align[2:4])
                        pstart = align[index_value]
                        if pstart > pend:
                            output[7] = pend-(output[1]-output[3])-1
                            output[8] = pstart-1
                        else:
                            output[7] = pstart
                            output[8] = pend+(output[1]-output[3])
                    else:        
                        index_value = align.index(min(align[2:4]))-2       
                        output[2] = min(align[2:4])-1
                        pstart = align[index_value]-1
                        if pstart > pend:
                            output[7] = pend-(output[1]-output[3])-1
                            output[8] = (pstart+1)+output[2]
                        else:
                            output[7] = pstart-output[2]
                            output[8] = pend+(output[1]-output[3])
                        output[2] = 0
                    output[3] = output[1]
                else:
                    index_value = alig1.index(min(alig1[2:4]))-2
                    output[2] = min(alig1[2:4])-1
                    pstart = alig1[index_value]-1
                    if align[6] == "100.0":
                        index_value = align.index(min(align[2:4]))-2
                        output[3] = min(align[2:4])-1
                        pend = align[index_value]-1
                        if pstart > pend:
                            output[7] = pend+1
                            output[8] = pstart+output[2]+1
                        else:
                            output[7] = pstart-output[2]
                            output[8] = pend
                    else:
                        index_value = align.index(max(align[2:4]))-2
                        output[3] = max(align[2:4])
                        pend = align[index_value]
                        if pstart > pend:
                            output[7] = pend-(output[1]-output[3])-1
                            output[8] = pstart+output[2]+1
                        else:
                            output[7] = pstart-output[2]
                            output[8] = pend+(output[1]-output[3])
                        output[3] = output[1]
                    output[2] = 0

            if output[7] > output[8]:
                echange = output[7]
                output[7] = output[8]-1
                output[8] = echange+1

            if output[7] < 0:
                output[7] = 0
            if output[8] > int(output[6]):
                output[8] = int(output[6])

            output[10] = output[3]-output[2] # alnL
            output[9] = output[10] # match
            output[11] = '60' # mapQ
            print('\t'.join(str(o) for o in output))
        else:
            if "100" not in output[12]:
                if hstart > hend:
                    if pstart > pend:
                        output[7] = pend-hend
                        output[8] = pstart+(output[1]-hstart)
                    else:
                        output[7] = pstart-(output[1]-hstart)
                        output[8] = pend+hend
                else:
                    if pstart > pend:
                        output[7] = pend-(output[1]-hend)
                        output[8] = pstart+hstart
                    else:
                        output[7] = pstart-hstart
                        output[8] = pend+(output[1]-hend)

                # start et end des haplotigs réinitialisés
                output[2] = 0
                output[3] = output[1]

                output[10] = output[3]-output[2] # alnL
                output[9] = output[10] # match
                output[11] = '60' # mapQ
                print('\t'.join(str(o) for o in output))
            else:
                # si 100% alors on fait une boite au début et/ou à la fin ...
                # ... s'il manque des portions
                output_list = []
                if hstart > hend:
                    if pstart > pend:
                        # manque portion au début
                        if hend > 1:
                            output[7] = pend-hend
                            output[8] = pend-1
                            output[2] = 0
                            output[3] = hend-1
                            output_list.append(output)
                        # manque portion à la fin
                        if hstart < output[1]:
                            output[7] = pstart
                            output[8] = pstart+(output[1]-hstart)
                            output[2] = hstart
                            output[3] = output[1]
                            output_list.append(output)
                    else:
                        if hend > 1:
                            output[7] = pstart-hend
                            output[8] = pstart-1
                            output[2] = 0
                            output[3] = hend-1
                            output_list.append(output)
                        if hstart < output[1]:
                            output[7] = pend
                            output[8] = pend+(output[1]-hstart)
                            output[2] = hstart
                            output[3] = output[1]
                            output_list.append(output)
                else:
                    if pstart > pend:
                        if hstart > 1:
                            output[7] = pend-hstart
                            output[8] = pend-1
                            output[2] = 0
                            output[3] = hstart-1
                            output_list.append(output)
                        if hend < output[1]:
                            output[7] = pstart
                            output[8] = pstart+(output[1]-hend)
                            output[2] = hend
                            output[3] = output[1]
                            output_list.append(output)
                    else:
                        if hstart > 1:
                            output[7] = pstart-hstart
                            output[8] = pstart-1
                            output[2] = 0
                            output[3] = hstart-1
                            output_list.append(output)
                        if hend < output[1]:
                            output[7] = pend
                            output[8] = pend+(output[1]-hend)
                            output[2] = hend
                            output[3] = output[1]
                            output_list.append(output)

                for out in output_list:
                    out[12] = "." # pas de score
                    out[10] = out[3]-out[2] # alnL
                    out[9] = out[10] # match
                    out[11] = '60' # mapQ
                    print('\t'.join(str(o) for o in output))
    else:
        # orientation
        output[4]='+' # ori
        hori = '+'
        pori = '+'
        if hstart > hend:
            hori = '-'
            output[2] = hend-1
            output[3] = hstart
        else:
            output[2] = hstart-1
            output[3] = hend
        if pstart > pend:
            pori = '-'
            output[7] = pend-1
            output[8] = pstart
        else:
            output[7] = pstart-1
            output[8] = pend

        if hori != pori:
            output[4]='-'

        main_start = output[2]
        main_end = output[3]

        output_start = output.copy()
        # création d'une nouvelle boite si le start de l'haplotig ...
        # ... n'est pas compris entre 0 et 100
        if main_start not in range(0, 100):
            if hstart < hend:
                if pstart < pend:
                    output_start[7] = pstart-hstart
                    output_start[8] = pstart-1
                else:
                    output_start[7] = pstart
                    output_start[8] = pstart+hstart
                output_start[2] = 0
                output_start[3] = hstart-1
            else:
                if pstart < pend:
                    output_start[7] = pend
                    output_start[8] = pend+hend
                else:
                    output_start[7] = pend-hend-1
                    output_start[8] = pend-1
                output_start[2] = 0
                output_start[3] = hend-1
            if output_start[8] in range(0, int(output_end[6])+1):
                if output_start[7] < 0:
                    output_start[7] = 0
                if output_end[8] > int(output_end[6]):
                    output_end[8] = int(output_end[6])
                output_start[10] = output_start[3]-output_start[2]
                output_start[9] = output_start[10]
                output_start[12] = '.' # no identity %
                output_start[11] = '60'
                print('\t'.join(str(o) for o in output_start))
        else:
            # recalcule le start s'il est égal à 0 plus ou moins 100 bases
            if hstart < hend:
                if pstart < pend:
                    output[7] = pstart-hstart
                else:
                    output[8] = pstart+hstart
            else:
                if pstart < pend:
                    output[8] = pend+hend
                else:
                    output[7] = pend-hend
            output[2] = 0
        
        output_end = output.copy()
        # création de la boite correspondant à la fin de l'haplotig si le end ...
        # ... n'est pas égal à la longueur de l'haplotig +/- 100 base
        if main_end not in range(output_end[1]-100, output_end[1]+100):
            if hstart < hend:
                if pstart < pend:
                    output_end[7] = pend
                    output_end[8] = pend+(output_end[1]-hend)
                else:
                    output_end[7] = pend-(output_end[1]-hend)-1
                    output_end[8] = pend-1
                output_end[2] = hend
                output_end[3] = output_end[1]
            else:
                if pstart < pend:
                    output_end[7] = pstart-(output_end[1]-hstart)-1
                    output_end[8] = pstart-1
                else:
                    output_end[7] = pstart
                    output_end[8] = pstart+(output_end[1]-hstart)
                output_end[2] = hstart
                output_end[3] = output_end[1]
            if output_end[7] in range(0, int(output_end[6])+1):
                if output_start[7] < 0:
                    output_start[7] = 0
                if output_end[8] > int(output_end[6]):
                    output_end[8] = int(output_end[6])
                output_end[10] = output_end[3]-output_end[2]
                output_end[9]= output_end[10]
                output_end[12] = '.' # no identity %
                output_end[11] = '60'
                print('\t'.join(str(o) for o in output_end))
        else:
            # calcul du end
            if hstart < hend:
                if pstart < pend:
                    output[8] = pend+(output[1]-hend)
                else:
                    output[7] = pend-(output[1]-hend)
            else:
                if pstart < pend:
                    output[7] = pstart-(output[1]-hstart)
                else:
                    output[8] = pstart+(output[1]-hstart)
            output[3] = output[1]

        if output[7] < 0:
            output[7] = 0
        if output[8] > int(output[6]):
            output[8] = int(output[6])

        # aln details, not real data
        output[10] = output[3]-output[2] # alnL
        output[9] = output[10] # match
        output[11] = '60' # mapQ 

        # print main part haplotig
        print('\t'.join(str(o) for o in output))