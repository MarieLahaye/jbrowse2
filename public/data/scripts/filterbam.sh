#!/bin/bash

# Fonction to display the script help
function usage {
	cat <<-__EOF__
		Usage:
			./filterbam.sh -i input_files -f fasta_file -o outdir [-h]

		Description:
			Filter BAM files based on regions ID given in FASTA file

		Arguments:
			-i, --input List of BAM files (required)
				Files must be separated by commas
				You can use the following command: ls path/to/*.bam | tr "\n" "," 
			-f, --fasta Fasta containing ID used for the filtration
			-o, --outdir Output directory  (required)
			-h, --help

		Exemple: ./filterbam.sh -i \`ls *.bam | tr "\n" ","\` -f file.fasta -o path/to/outdir
		__EOF__
}

# Eval command line arguments given in input
ARGS=$(getopt -o "i:f:o:h" --long "input:,fasta:,outdir:,help" -- $@ 2> /dev/null)

# Check if the return code of the previous command is not equal to 0 (command ...
# ... didn't work)
# >&2 send the message to standard error (stderr) instead of standard out (stdout)
if [ $? -ne 0 ]; then
	echo "Error in the argument list. Use -h or --help to display the help." >&2
	exit 1
fi

eval set -- ${ARGS}
while true
do
	case $1 in
		-i|--input)
			INPUT_FILES=$2
			shift 2
			;;
		-o|--outdir)
			OUTPUT_DIRECTORY=$2
			shift 2
			;;
		-f|--fasta)
			FASTA_FILE=$2
			shift 2
			;;
		-h|--help)
			usage
			exit 0
			;;
		--) shift
			break
			;;
		*)	echo "Option $1 is not recognized. Use -h or --help to display the help." && \
			exit 1
			;;
	esac
done

# Check if one required parameter is missing in the given command line
if [[ ${INPUT_FILES} == "" ]] || [[ ${FASTA_FILE} == "" ]] || [[ ${OUTPUT_DIRECTORY} == "" ]]; then
	echo "Options --input, --fasta and --outdir are required. Use -h or --help to display the help" >&2
	exit 1
fi

# Check if FASTA exist
if ! [ -f "${FASTA_FILE}" ]; then
	echo "${FASTA_FILE} doesn't exist. Create it or enter the right path and file name" >&2
	exit 1
fi

# Check if directory exists
if ! [ -d "${OUTPUT_DIRECTORY}" ]; then
    echo "${OUTPUT_DIRECTORY} doesn't exist. Create it or enter the right path and file name" >&2
    exit 1
fi

for i in $(echo ${INPUT_FILES} | tr "," "\n")
do
	if [ -f $i ]; then
		file=${i##*/}
		file_name=${file%%.*}

		echo "Filtering $i..."
		samtools view -b $i `grep ">" ${FASTA_FILE} | sed 's/>//g'` > ${file_name}.sorted.filtered.bam
	else
		echo "File doesn't exist. Create it or enter the right path and file name" >&2
	fi
done
