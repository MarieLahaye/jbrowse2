#!/usr/bin/env python3
# coding: utf-8
import argparse

def command_line():
    parser = argparse.ArgumentParser(description='''
        Replace chromStart and chromEnd by removing the maximum overhang
        of the junctions to keep the real start and end positions
        ''')
    parser.add_argument('-i', '--input', required=True, help='Original BED file')
    parser.add_argument('-o', '--output', required=True, help='Output file')
    args = parser.parse_args()
    return args.input, args.output


fInput, fOutput = command_line()
input_file = open(fInput, 'r')
output_file = open(fOutput, 'w')
for line in input_file:
    lineList = line.split('\t')
    # filtre les jonctions supportés par 1 ou 2 reads
    if int(lineList[4]) > 2:
        # calcul des bons start et end
        start = int(lineList[1]) + int(lineList[10].split(',')[0])
        end = int(lineList[2]) - int(lineList[10].split(',')[1])
        lineList[1] = str(start)
        lineList[2] = str(end)
        output_file.write('\t'.join(lineList))
input_file.close()
output_file.close()