#!/usr/bin/env python3
# coding: utf-8
import argparse


def command_line():
    parser = argparse.ArgumentParser(description='''
        Create 'name' column in BED file by concatenating
        the 3 other columns
        ''')
    parser.add_argument('-i', '--input', required=True, help='BED file')
    parser.add_argument('-o', '--output', required=True, help='Output file')
    args = parser.parse_args()
    return args.input, args.output


fInput, fOutput = command_line()
input_file = open(fInput, 'r')
output_file = open(fOutput, 'w')
c = 1
for line in input_file:
    output_file.write(line[:-1]+"\t"+str(c)+"\n")
    c += 1
input_file.close()
output_file.close()
