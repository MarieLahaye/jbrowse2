# il manque l'information GeneID à chaque ligne du gff pour que featureCounts fonctionne bien et trouve tous les exons de chaque gène

#for gene in `awk '$3 == "gene" {print $0}' RI_annotations.gff3 | sed 's/.*ID=//' | sed 's/;.*//' | sed 's/gene://'`
#do
#grep -w $gene RI_annotations.gff3 | sed "s/$/;GeneID=$gene/" >> RI_annotations_final.gff3
#done

/data2/malahaye/git_jbrowse2/public/data/scripts/add_geneID_gff.py -i RI_annotations.gff3 -o RI_annotations_final.gff3

# 1. comptage du nombre de gènes
# il y a le même entre le GFF3 original et celui que je formate
# par contre il manque 2282 gènes dans le GFF que j'utilisais avant ... il faut relancer GREAT du début.
awk '$3 == "gene" {print $0}' RI_annotations.gff3  | sed 's/.*ID=//' | sed 's/;.*//' | wc -l
# 54831
awk '$3 == "gene" {print $0}' RI_annotations_final.gff3 | sed 's/.*ID=//' | sed 's/;.*//' | wc -l
# 54831

source activate /cm/shared/apps/miniconda3/GREAT_environment
export PERL5LIB=/cm/shared/apps/miniconda3/lib/perl5/5.22.0/

STAR --runThreadN 5 --runMode genomeGenerate --genomeDir index_RI --genomeFastaFiles ../Riesling_pseudomolecules_ctgP.fa --sjdbGTFfile RI_annotations_final.gff3 --sjdbGTFtagExonParentTranscript Parent --sjdbGTFtagExonParentGene GeneID

wc -l index_RI/geneInfo.tab
# 53355

# check si des mRNA n'ont pas été indexés
for gene in `awk '$3 == "gene" {print $0}' ../RI_annotations_final.gff3  | sed 's/.*ID=//' | sed 's/gene://'`
do
if grep --quiet "$gene" index_RI/geneInfo.tab; then
  continue
else
	if [ "$(echo `grep "$gene" ../RI_annotations_final.gff3 | tail -n1 | cut -f3`)" == "mRNA" ]; then
	  	echo "$gene not found in index"
	  	echo `grep "$gene" ../RI_annotations_final.gff3 | tail -n1 | cut -f3`
	fi
fi
done

# ok
